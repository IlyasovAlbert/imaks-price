<?php

namespace app\models;

class Product extends \yii\db\ActiveRecord
{
    const STATE_ACTIVE  = 1;
    const STATE_DISABLE = 2;
    const STATE_SKIP    = 3;

	/**
	* id
	* catId
	* fileId
	* supplierId
	* state
	* artikul
	* name
	* price
    * qty
    * createdAt
	* updatedAt
	**/
	public function getCategory()
	{
		return $this->hasOne(Category::className(), ['id' => 'catId']);
	}
}
