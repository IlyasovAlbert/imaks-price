<?php

namespace app\models;

class ProductModel extends \yii\base\BaseObject
{
	/**
	* id
    * fileId
    * reason
	* artikul
	* name
	* price
    * qty
	**/
    // public $importStat = [
    //     'total' => null,
    //     'skipped' => null,
    //     'imported' => null,
    // ];

    public static function get(int $id)
    {
        return Product::findOne($id);
    }

    public function attributeLabels()
    {
        return [
            'artikul'      => 'Артикул',
            'name'         => 'Наименование',
            'price'        => 'Цена',
            'qty'          => 'Кол-во',
            'supplierId'   => 'ID поставщика',
            'catId'        => 'ID категории',

            //left join
            'supplierName' => 'Поставщик',
            'categoryName' => 'Категория',
        ];
    }

    public function getBySupplierId(int $supplierId)
    {
        return Product::find(['supplierId' => $supplierId])->joinWith('category')->all();
    }

    public static function getByArtikul(string $artikul, int $supplierId=0)
    {
        $conditions['artikul'] = $artikul;
        if ($supplierId) {
            $conditions['supplierId'] = $supplierId;
        }
        return Product::findOne($conditions);
    }

    public function add(
        \app\models\PriceFile $file,
        int $state,
        $artikul,
        $name,
        $price,
        $qty,
        $qtyDefault = null
    )
    {
//        var_dump($artikul, $name, $price, $qty, $qtyDefault);
        //if all empty, then nothing to do
        if (!$artikul && !$name && !$price && !$qty) {
            return;
        }

        $artikul = trim($artikul);
        $name    = trim($name);
        $priceOriginal   = $price;
        $qtyOriginal     = $qty;
        
        $price = $this->normalizeNumber($priceOriginal);
        $qty   = $this->normalizeNumber($qtyOriginal);
        
        $qty   = $qtyDefault && !is_numeric($qty) ? $qtyDefault : $qty;
//        echo "$artikuk<br>$name<br>$price<br>$qty<p>";

        $reasonSkip = null;
        if ($state == Product::STATE_SKIP) {
            $reasonSkip  = ProductSkipped::REASON_MANUAL_SKIP;
        //all fields is required
        } elseif (!($artikul && $name && $price && $qty)) {
            $reasonSkip  = ProductSkipped::REASON_FIELD_EMPTY;
        } elseif (!(is_numeric($price) && is_numeric($qty))) {
            $reasonSkip  = ProductSkipped::REASON_TYPE_ERROR;
        } elseif ($this->getByArtikul($artikul, $file->supplierId)) {
            $reasonSkip  = ProductSkipped::REASON_ARTIKUL_EXIST;
        }

        if ($reasonSkip) {
            $productSkipped = new ProductSkipped;
            $productSkipped->fileId  = $file->id;
            $productSkipped->reason  = $reasonSkip;
            $productSkipped->artikul = $artikul;
            $productSkipped->name    = $name;
            $productSkipped->price   = $priceOriginal;
            $productSkipped->qty     = $qtyOriginal;
            $productSkipped->save();
            // $this->importStat['skipped']++;
            return $productSkipped;
        }

        $categoryId = CategorySupplierArtikul::findOne([
                'supplierId' => $file->supplierId,
                'artikul'    => $artikul,
            ])['categoryId'];
        $product = new Product;
        $product->fileId  = $file->id;
        $product->supplierId  = $file->supplierId;
        $product->state   = $state;
        $product->artikul = $artikul;
        $product->name    = $name;
        $product->price   = $price;
        $product->qty     = $qty;
        $categoryId ? $product->catId = $categoryId : null;
        $product->save();

        (new \app\models\CategoryModel)->setStat($product->catId);
        // $this->importStat['imported']++;
        return $product;
    }

    public static function truncate(int $supplierId)
    {
        $categoryModel = new CategoryModel;
        $products = (new \yii\db\Query())
            ->select([
                'catId',
            ])
            ->from(Product::tableName())
            ->andWhere(['supplierId' => $supplierId])
            ->andWhere(['>', 'catId', 0])
            ->distinct()
            ->all();
        \Yii::$app->db->createCommand()->delete(Product::tableName(), ['supplierId' => $supplierId])->execute();
        foreach ($products as $product) {
            $categoryModel->setStat($product['catId']);
        }
    }

    public function importFile(\app\models\PriceFile $file)
    {
        // $this->importStat = [];
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $this->truncate($file->supplierId);
            $i=-1;
            foreach ($file->getData() as $row) {
                if (++$i < $file->fileLineDataStart) {
                    continue;
                }

                $this->add(
                    $file,
                    \app\models\Product::STATE_ACTIVE,
                    $row[$file->fileColumnArtikul],
                    $row[$file->fileColumnName],
                    $row[$file->fileColumnPrice],
                    $row[$file->fileColumnQty],
                    $file->fileQtyDefault
                );
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            echo $e->getMessage();
            return false;
        }

        return true;
    }

    public function setCatByName($catName)
    {
        $cat = Category::findOne(['name' => $catName]);
    }

/////////////////////////////////////////////////////////
//    public function getReasonSkip(
//        \app\models\PriceFile $file,
//        int $state,
//        $artikul,
//        $name,
//        $price,
//        $qty
//    )
//    {
//        $reasonSkip = null;
//        if ($state == Product::STATE_SKIP) {
//            $reasonSkip  = ProductSkipped::REASON_MANUAL_SKIP;
//        } elseif (!(is_numeric($price) && is_numeric($qty))) {
//            $reasonSkip  = ProductSkipped::REASON_TYPE_ERROR;
//        } elseif ($this->getByArtikul($artikul, $file->supplierId)) {
//            $reasonSkip  = ProductSkipped::REASON_ARTIKUL_EXIST;
//        }
//
//        return $reasonSkip;
//    }
//
//    public function importFile123(\app\models\PriceFile $file)
//    {
//        echo __METHOD__;
//        $state = \app\models\Product::STATE_ACTIVE;
//        $time1 = time();
//        $products = [];
//        $transaction = \Yii::$app->db->beginTransaction();
//
//        $command = \Yii::$app->db->createCommand();
////         ->insert('user', [
////     'name' => 'Sam',
////     'age' => 30,
//// ])->execute();
//
//        try {
//            $this->truncate($file->supplierId);
//            $i=-1;
//            foreach ($file->getData() as $row) {
//                if (++$i < $file->fileLineDataStart) {
//                    continue;
//                }
//
//                $artikul = $row[$file->fileColumnArtikul];
//                $name = $row[$file->fileColumnName];
//                $price = $row[$file->fileColumnPrice];
//                $qty = $row[$file->fileColumnQty];
//
//                if (!$artikul && !$name && !$price && !$qty) {
//                    continue;
//                }
//                
//                $reasonSkip = ProductSkipped::REASON_ARTIKUL_EXIST; 
//                // $this->getReasonSkip(
//                //     $file,
//                //     $state,
//                //     $artikul,
//                //     $name,
//                //     $price,
//                //     $qty
//                // );
//                if ($reasonSkip) {
//                    $command->insert('product_skipped', [
//                        'fileId'  => $file->id,
//                        'reason'  => $reasonSkip,
//                        'artikul' => $artikul,
//                        'name'    => $name,
//                        'price'   => $price,
//                        'qty'     => $qty,
//                    ])->execute();
//                    continue;
//                }
//
//                // $product = new Product;
//                // $product->fileId  = $file->id;
//                // $product->supplierId  = $file->supplierId;
//                // $product->state   = $state;
//                // $product->artikul = $artikul;
//                // $product->name    = $name;
//                // $product->price   = $price;
//                // $product->qty     = $qty;
//                // $product->save();
//                // return $product->id;
//            }
//            // echo '<br>';
//            // break;
//            $transaction->commit();
//        } catch (\Exception $e) {
//            $transaction->rollBack();
//            // echo $e->getMessage();
//            print_r($a);
//        }
//        echo time()-$time1;
//        echo '<br>';
//    }

    public function normalizeNumber($num)
    {
        $num = trim($num);

        $templ = '[,\s\'"]';
        if (preg_match('/^\d+,{1}\d+$/', $num)) {
            $num = str_replace(',', '.', $num);
        } elseif (preg_match('/^.*'.$templ.'+.+\.{1}\d+$/', $num)) {
            $num = preg_replace('/'.$templ.'/', '', $num);
        } elseif (preg_match('/^.*'.$templ.'+\d+$/', $num)) {
            $num = preg_replace('/'.$templ.'/', '', $num);
        }

        // $num = (string)(float)$num === (string)$num ? (float)$num : $num;

        return (string)$num;
    }
}
