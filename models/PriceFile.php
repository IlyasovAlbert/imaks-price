<?php

namespace app\models;

class PriceFile extends \yii\db\ActiveRecord
{
	//файл только загружен
    const STATUS_RAW          = 1;
    //структура файла непроверена
    const STATUS_NEED_CONFIRM = 2;
    //структура файла утверждена
    const STATUS_READY        = 3;
    //файл импортирован
    const STATUS_IMPORTED     = 4;

    const PRICE_DIR = 'uploads/prices';

    const MAX_SIZE_MB = 3;

	/**
	* id
	* supplierId
	* status
	* fileLineDataStart
	* fileColumnArtikul
	* fileColumnPrice
	* fileColumnQty
	* createdAt
	* updatedAt
	**/
    private $path;


    private $stat = [
        'total'      => null,
        'imported'   => null,
        'skipped'    => null,
        'calculated' => null,
    ];

    public function getInfo()
    {
        if (!$this->stat['calculated']) {
            $conditions = ['fileId' => $this->id];
            $this->stat['imported'] = Product::find()->where($conditions)->count();
            $this->stat['skipped'] = ProductSkipped::find()->where($conditions)->count();
            $this->stat['total'] = $this->stat['imported'] + $this->stat['skipped'];
            $this->stat['calculated'] = true;
        }
        return ['stat' => $this->stat];
    }

    public function __construct(int $id = null)
    {
        $this->path = \Yii::$app->basePath . '/' . self::PRICE_DIR;

        parent::__construct();
        if ($id) {
        	 $this->findOne(['id' => $id]);
        	 return $this;
        }

        return $this;
    }

    public function get(int $id)
    {
        return self::findOne($id);
    }

    //doesn't work
    //ZipArchive Object ( [status] => 0 [statusSys] => 0 [numFiles] => 0 [filename] => [comment] => )
    // public function createZip(string $zipName, string $file)
    // {
    //     echo $name = \Yii::$app->basePath . '\\' . self::PRICE_DIR . $zipName . '.zip';
    //     $zip = new \ZipArchive($name);
    //     // $zip->open(\ZipArchive::CREATE);
    //     // $zip->addFile($file);
    //     print_r($zip);
    //     $zip->close();

    //     return;
    // }

    public function priceFileAdd(\app\models\SupplierPriceUpload $form)
    {
        $this->name       = $form->price->name;
        $this->supplierId = $form->supplierId;
        $this->status = self::STATUS_RAW;
        $this->save();

        $this->saveFile($form->price->tempName);
        return $this->id;
    }

    public function getExtension()
    {
        return pathinfo($this->name, PATHINFO_EXTENSION);
    }

    public function saveFile(string $file): void
    {
        move_uploaded_file($file, $this->path . '/' .  $this->id . '.'.$this->getExtension());
    }

    public function getData()
    {
        if (!preg_match('/.*\.csv$/', $this->name)) {
            return $this->getExcelData();
        }
        $data = new \SplFileObject($this->path . '/' .  $this->id . '.csv');
        $data->setFlags(\SplFileObject::READ_CSV);
        return $data;
    }

    public function getExcelData(int $rowStart = 1, int $limit = 50000)
    {
        $ext  = $this->getExtension();
        $path = $this->path . '/' .  $this->id . '.'.$ext;

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
        $sheet = $spreadsheet->getActiveSheet();
        return $sheet->rangeToArray('A'.$rowStart.':M'.($rowStart + $limit));
    }

    public function setConfig($columns)
    {
        $this->fileColumnArtikul = $columns['fileColumnArtikul'];
        $this->fileColumnName    = $columns['fileColumnName'];
        $this->fileColumnPrice   = $columns['fileColumnPrice'];
        $this->fileColumnQty     = $columns['fileColumnQty'];
        $this->fileLineDataStart = $columns['fileLineDataStart'];
        $this->fileQtyDefault    = $columns['fileQtyDefault'];
        return $this;
    }

    public function isImported()
    {
        return $this->status == self::STATUS_IMPORTED;
    }

    public function isLearned()
    {
        return $this->status != self::STATUS_RAW;
    }

    // public function isConfirmed()
    // {
    //     return $this->status != self::STATUS_RAW && $this->status != self::STATUS_NEED_CONFIRM;
    // }

    public function isReady()
    {
        return $this->status == self::STATUS_READY;
    }
}
