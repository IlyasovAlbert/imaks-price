<?php

namespace app\models;

class CategorySupplierArtikulModel extends \yii\base\Model
{
	/**
	* create - $artikul, $supplierId, $categoryId, null
	* update - $artikul, $supplierId, $categoryId, $oldCategoryId
	* delete - $artikul, $supplierId, null, $oldCategoryId
	*/
	public static function update(string $artikul, int $supplierId, $categoryId, $oldCategoryId)
	{
		if (!($artikul && $supplierId && ($categoryId || $oldCategoryId))) {
			return false;
		}
		$row = \app\models\CategorySupplierArtikul::findOne([
            'artikul'    => $artikul,
            'supplierId' => $supplierId,
            'categoryId' => $oldCategoryId,
        ]);

		//удаление
        if (!$categoryId) {
        	$row && $row->delete();
        	return true;
        //ошибочное добавление
        } elseif (!$row && $oldCategoryId) {
        	return false;
        }

        $newRow = \app\models\CategorySupplierArtikul::findOne([
            'artikul'    => $artikul,
            'supplierId' => $supplierId,
            'categoryId' => $categoryId,
        ]);
        //добавление
        if (!$row) {
        	if ($newRow) {
        		return false;
	        }
        	$row = new \app\models\CategorySupplierArtikul;
            $row->artikul    = $artikul;
            $row->supplierId = $supplierId;
        } elseif ($row && $newRow) {
        	$row->delete();
        	return true;
        }

        $row->categoryId = $categoryId;
        $row->save();

        return true;
	}
}
