<?php

namespace app\models;

class SupplierForm extends \yii\base\Model
{
    public $id;
    public $name;
    public $delivery;
    public $deliveryTime;
    public $deliveryCost;
    public $deliveryFree;
    public $pickup;
    public $discount;
    public $addresses;
    public $notes;

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'isNameUnique'],
            [['delivery', 'deliveryTime', 'deliveryCost', 'deliveryFree', 'pickup', 'discount', 'addresses', 'notes'], 'trim'],
        ];
    }

    public function isNameUnique($attribute, $params, $validator)
    {
        $name = $this->$attribute;
        $supplier = \app\models\Supplier::getByName($name);
        if ($supplier && $supplier->id != $this->id) {
            $this->addError($attribute, 'Поставщик с наименованием "' . $name . '" уже существует');
        }
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Наименование',
            'delivery' => 'Доставка',
            'deliveryTime' => 'Срок доставки, дней',
            'deliveryCost' => 'Стоимость доставки, рублей',
            'deliveryFree' => 'Бесплатная доставка при сумме заказа от, рублей',
            'pickup' => 'Самовывоз',
            'discount' => 'Скидка, %',
            'addresses' => 'Адреса',
            'notes' => 'Заметки',

            'createdAt' => 'Создано',
            'updatedAt' => 'Изменено',
        ];
    }
}
