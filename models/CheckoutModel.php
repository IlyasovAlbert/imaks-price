<?php

namespace app\models;

class CheckoutModel extends \yii\base\BaseObject
{
	/**
	* @param array $data [Product object1, Product object 2]
	* @return array [categoryId => [supplierId1, supplierId2]]
	*/
	public function getSuppliersWithMinimalPrice(array $products): array
	{
		// $categorysData = [];
		$categorySuppliersPrices = [];
		$categoryPrices = [];
        foreach ($products as $item) {
            // $categorysData[$item->catId][$item->supplierId] = $item->price;
            $categorySuppliersPrices[$item->catId][$item->price][] = $item->supplierId;
            $categoryPrices[$item->catId][] = $item->price;
        }

        // print_r($categorysData);
        // print_r($categoryPrices);
        // print_r($categorySuppliersPrices);

        //поиск минимальной цены
        $minPrices = [];
        foreach ($categoryPrices as $categoryId => $prices) {
        	$minPrice = min($prices);
        	$minPrices[$categoryId] = $categorySuppliersPrices[$categoryId][$minPrice];
        }
        // array_search(needle, haystack)
//         (print_r($minPrices));
// die;
        return $minPrices;
	}

	/**
	*/
	public function getSuppliersCombination(array $products)
	{
		// die($this->generateSql());
		$supplierIds = [];
		$categoryIds = [];
		$prices = [];
		//этап 0 - получить массив из ID поставщиков
		foreach ($products as $product) {
			$supplierIds[] = $product->supplierId;
			$categoryIds[] = $product->catId;
            $prices[$product->supplierId][$product->catId] = $product->price;
            $pricesSuppliers[$product->supplierId][$product->catId] = $product->price;
		}
		$supplierIds = array_values(array_unique($supplierIds));
		$categoryIds = array_values(array_unique($categoryIds));
		// print_r($prices);die;
		// return $supplierIds;


		$deliveryCost = [];
		$deliveryFree = [];
		foreach ((new Supplier)->findAll(['id' => $supplierIds]) as $supplier) {
			$deliveryCost[$supplier->id] = $supplier->deliveryCost;
			$deliveryFree[$supplier->id] = $supplier->deliveryFree;
		}
		print_r($deliveryCost);
		print_r($deliveryFree);
		die;

		//этап 1 - создать комбинацию из ID поставщиков
		// if ($i == 0) {

		// 		} elseif (0) {

		// 		}
				//одиночная комбинация
		$comb1 = $this->comb1();

		//этап 2 - из массива продуктов убрать те, ID поставщиков которых не входят в массив из этапа 2

		//этап 3 - убедиться, что есть все товары. Если нет, то отбросить текущую комбинацию.
	}

	//одиночная комбинация
	/*
	prices [supplierId=>categoryId]
	*/
	public function comb1($supplierIds, $categoryIds, $deliveryCost, $deliveryFree, $prices)
	{
		// $deltaOfDeliveryCost
		// $deltaOfDeliveryFree
		// $amount	
		// мысли
		// лучший результат - это сумма - это когда бесплатная доставка и выбрана минимальная цена из всех поставщиков. Следующие итерации должны проверяться на вероятность получение меньшей суммы на 500р. Если вероятность экономии 500р низкая, то итерацию пропускать и приступать к следующий
		for ($i = 0; $i < count($supplierIds); $i++) {
					for ($j = 1; $j < count($supplierIds); $j++) {
						
					}
				}
	}

	/**
	*28-37 - Т1-Т10
	32-39 - П1-П8
	*/
	public function generateSql()
	{
		$sql = 'INSERT INTO products (supplierId, catId, state, artikul, name, qty, price) VALUES ';
		for ($supplierId = 32; $supplierId <= 39; $supplierId++) {
			for ($categoryId = 28; $categoryId <= 37; $categoryId++) {
				$sql .= "\r\n($supplierId, $categoryId, 1, '$supplierId-$categoryId', 'Товар $supplierId-$categoryId', 999, ".rand(10, 900)."),";
			}
		}
		return $sql;
	}

	public function compileSpreadsheet($supplier, $productIds, $pickup = 0)
	{
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $i = 1;

		if (!($supplier && is_array($productIds) && count($productIds))) {
        	$sheet->setCellValue('A'.$i, 'Ошибка! Не получены все необходимые данные.');
        	return $spreadsheet;
		}

        $discount     = (float) $supplier->discount;
        $deliveryFree = (float) $supplier->deliveryFree;
        $deliveryCost = (float) $supplier->deliveryCost;

        $discountKoeff = 1 - $discount/100;

        $discountCell = $amountCell = $deliveryCell = $totalCell = null;

        $sheet->setCellValue('A'.$i, 'Дата:');
        $sheet->setCellValue('B'.$i, date('d.m.Y H:i:s'));
        $i++;
        $sheet->setCellValue('A'.$i, 'Поставщик:');
        $sheet->setCellValue('B'.$i, $supplier->name);
        $i++;
        $sheet->setCellValue('A'.$i, 'Скидка, %:');
        $sheet->setCellValue($discountCell = 'B'.$i, $discount);
        $i++;
        $sheet->setCellValue('A'.$i, 'Стоимость товаров:');
        $sheet->setCellValue($amountCell   = 'B'.$i, 0);
        $i++;
        $sheet->setCellValue('A'.$i, 'Доставка:');
        $sheet->setCellValue(
        	$deliveryCell = 'B'.$i,
        	$pickup
        		? 0
        		: '=if('.$amountCell .'>='.$deliveryFree.', 0, '.$deliveryCost.')'
        );
        $sheet->setCellValue('C'.$i, $pickup ? 'самовывоз' : '');
        $i++;
        $sheet->setCellValue('A'.$i, 'Итого:');
        $sheet->setCellValue($totalCell    = 'B'.$i, '='.$amountCell.'+'.$deliveryCell);
        $i++;
        $sheet->setCellValue('A'.$i, '');
        $i++;
        $sheet->setCellValue('A'.$i, 'Артикул');
        $sheet->setCellValue('B'.$i, 'Наименование');
        $sheet->setCellValue(($priceColumn  = 'C') . $i, 'Цена за ед. изм. с учётом скидки');
        $sheet->setCellValue(($qtyColumn    = 'D') . $i, 'Кол-во');
        $sheet->setCellValue(($amountColumn = 'E') . $i, 'Итого');
        $i++;

        $productsLineStart = $i;
        $sheet->setCellValue('A'.$i, 'Ошибка! Товары не выбраны.');
        foreach (\app\models\Product::find()->where(['id' => array_keys($productIds), 'supplierId' => $supplier->id])->all() as $product) {
            $sheet->setCellValue('A'.$i, $product->artikul);
            $sheet->setCellValue('B'.$i, $product->name);
            $sheet->setCellValue($priceColumn  . $i, '=(1 - ' . $discountCell . '/100) * ' . $product->price);
            $sheet->setCellValue($qtyColumn    . $i, $productIds[$product->id]);
            $sheet->setCellValue($amountColumn . $i, '='.$priceColumn.$i.'*'.$qtyColumn.$i);

            $i++;
        }
        $productsLineEnd = $i-1;
        $sheet->setCellValue($amountCell, '=sum('.$amountColumn.$productsLineStart.':'.$amountColumn.$productsLineEnd.')');

        return $spreadsheet;
	}

	public function saveSpreadsheetToFile($spreadsheet, $filenameWoExt)
	{
        $writerType = 'Xls';
        $filename = $filenameWoExt.'.'.strtolower($writerType);
        $path = 'php://output';
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, $writerType);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer->save($path);
	}
}
