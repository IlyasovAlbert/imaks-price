<?php

namespace app\models;

class CookieModel implements SaverInterface
{
    protected $namespace;
    protected $data = [];

    public function __construct(string $namespace)//: CookieModel
    {
        $namespace = trim($namespace);
        if (!$namespace) {
            throw new \Exception('Cookie namespace is need');
        }
        $this->setNamespace($namespace);
            // print_r($_COOKIE['data']);

        if (isset($_COOKIE['data'][$namespace])) {
            $data = json_decode($_COOKIE['data'][$namespace], true);
            // echo 'cookie with namespace has: '; print_r($data);
            $this->data[$namespace] = $data;
        }
        // print_r($_COOKIE);

        return $this;
    }

    public function setNamespace(string $namespace): CookieModel
    {
        $this->namespace = $namespace;
        !array_key_exists($namespace, $this->data) ? $this->data[$namespace] = [] : null;
        return $this;
    }

    public function getNamespace(): string
    {
        return $this->namespace;
    }

    public function set(string $name, $value): CookieModel
    {
        $namespace = $this->getNamespace();

        if (null === $value) {
            unset($this->data[$namespace][$name]);
        } else {
            // echo '$name='.$name; echo 'value='; print_r($value);
            // $data = is_array($data) ? $data : [$data];
            $this->data[$namespace][$name] = $value;
        }
        // $_COOKIE['data'][$namespace] = $this->data[$namespace];

        // echo 'get()='; print_r($this->get());
        $cookieName = sprintf('data[%s]', $namespace);
        $cookieVal  = json_encode($this->get(), JSON_UNESCAPED_UNICODE);
        // echo '$cookieName='; print_r($cookieName);
        // echo '$cookieVal='; print_r($cookieVal);
        setcookie($cookieName, $cookieVal, time()+365*24*3600, "/");
        return $this;
    }

    public function get(string $key = null)
    {
        // throw new \Exception(__METHOD__);
        $namespace = $this->getNamespace();

        if ($key && !array_key_exists($key, $this->data[$namespace])) {
            return null;
        }
        return $key ? $this->data[$namespace][$key] : $this->data[$namespace];
    }
}
