<?php

namespace app\models;

class ProductCatForm extends \yii\base\Model
{
    public $catName;

    public function rules()
    {
        return [
            ['catName', 'trim']
        ];
    }

    public function attributeLabels()
    {
        return [
            'fileColumnArtikul' => 'Артикул',
        ];
    }
}
