<?php

namespace app\models;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        // '100' => [
        //     'id' => '100',
        //     'username' => 'admin',
        //     'password' => 'admin',
        //     'authKey' => 'test100key',
        //     'accessToken' => '100-token',
        // ],
        // '101' => [
        //     'id' => '101',
        //     'username' => 'demo',
        //     'password' => 'demo',
        //     'authKey' => 'test101key',
        //     'accessToken' => '101-token',
        // ],
        100 => [
            'id' => 100,
            'username' => 'sjckaekw',
            'password' => 'EMyZGbvhgSTtgxKC',
            'authKey' => 'YAKlFlRuaxI8zqOEVifC' . 100 . 'key',
            'accessToken' => 'Ax4HtQgGOzYxUAR19IhR' . 100 .'-token',
        ],
        101 => [
            'id' => 101,
            'username' => 'kbcommjl',
            'password' => 'EIvAevNF82c4CW8I',
            'authKey' => 'CMFS6LeNmmlfQcvUEHVq' . 101 . 'key',
            'accessToken' => 'BZN4sUojFgCzmh2kWLOv' . 101 .'-token',
        ],
        102 => [
            'id' => 102,
            'username' => 'crgxnvav',
            'password' => 'uYcSbTFYhCmlPtK0',
            'authKey' => '8qpZl6f32waAcdcN50zR' . 102 . 'key',
            'accessToken' => '0olWFhTXZUb0Cw1Qmiye' . 102 .'-token',
        ],
        103 => [
            'id' => 103,
            'username' => 'xyyphmoe',
            'password' => 'kkEHjC5PYpuIW5Jf',
            'authKey' => 'JUnBChvpmOg1uRdyGepg' . 103 . 'key',
            'accessToken' => 'zmqpSguQR32A9W5vqiC9' . 103 .'-token',
        ],
        104 => [
            'id' => 104,
            'username' => 'cmzxbwmd',
            'password' => 'GJcDyeZE9in0sre6',
            'authKey' => 'F5N2D0Gln9CcZpbly9SD' . 104 . 'key',
            'accessToken' => 'xT9fUX7r4KvLXjtGaxj8' . 104 .'-token',
        ],
        105 => [
            'id' => 105,
            'username' => 'ezxnrhru',
            'password' => '1sOtNzzFC20k2IL8',
            'authKey' => '0hpw97YdmHawIIFeYW5I' . 105 . 'key',
            'accessToken' => 'qxrewXh7Z6XvM5aEyLPR' . 105 .'-token',
        ],
        106 => [
            'id' => 106,
            'username' => 'bctlznbp',
            'password' => 'hKC3C18cjReM9S1d',
            'authKey' => 'Y6ssZE6Z41s7xDI4Sf2l' . 106 . 'key',
            'accessToken' => 'cbY5EVz0IpWDZ9D8KQZN' . 106 .'-token',
        ],
        107 => [
            'id' => 107,
            'username' => 'pqkzgkdb',
            'password' => 'j1D0CrzQ7QaPylTv',
            'authKey' => 'oClDD2gNGoQ9BxWKWevA' . 107 . 'key',
            'accessToken' => 'FbYxl5PQcW090ZNRNHj7' . 107 .'-token',
        ],
        108 => [
            'id' => 108,
            'username' => 'xamiismn',
            'password' => 'f4Um14al61EcxHYG',
            'authKey' => 'y85EngIumaBmkqBQc7Ly' . 108 . 'key',
            'accessToken' => 'afG0tb2eLqQVC4vBesDt' . 108 .'-token',
        ],
    ];


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
