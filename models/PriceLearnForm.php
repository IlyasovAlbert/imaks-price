<?php

namespace app\models;

class PriceLearnForm extends \yii\base\Model
{
    public $fileColumnArtikul;
    public $fileColumnName;
    public $fileColumnPrice;
    public $fileColumnQty;
    public $fileLineDataStart;
    public $fileQtyDefault;

    public function rules()
    {
        return [
            [['fileColumnArtikul', 'fileColumnName', 'fileColumnPrice', 'fileColumnQty', 'fileLineDataStart'], 'required'],
            ['fileQtyDefault', 'integer', 'min' => 1, 'max' => 65535]
        ];
    }

    public function attributeLabels()
    {
        return [
            'fileColumnArtikul' => 'Артикул',
            'fileColumnName'    => 'Наименование',
            'fileColumnPrice'   => 'Цена',
            'fileColumnQty'     => 'Кол-во',
            'fileLineDataStart' => 'Начало данных',
            'fileQtyDefault'    => 'Кол-во по-умолчанию',
        ];
    }

    public function isValsUnique()
    {
        $labels = $this->attributeLabels();
        unset($labels['fileLineDataStart']);
        $fields = array_keys($labels);
        
        $pairs = [];
        for ($i = 0; $i<count($fields); ) {
            $field1 = $fields[$i];
            for ($j = ++$i; $j<count($fields); $j++) {
                $pairs[] = [$field1, $fields[$j]];
            }
        }

        $msg = 'Значение поля "%s" не должно совпадать со значением поля "%s"';
        foreach ($pairs as $pair) {
            $field1 = $pair[0];
            $field2 = $pair[1];
            if ($this->$field1 == $this->$field2) {
                $this->addError($field1, sprintf($msg, $labels[$field1], $labels[$field2]));
                $this->addError($field2, sprintf($msg, $labels[$field2], $labels[$field1]));
            }
        }

        return !$this->hasErrors();
    }
}
