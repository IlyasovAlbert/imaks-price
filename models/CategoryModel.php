<?php

namespace app\models;

class CategoryModel extends \yii\base\BaseObject
{
	/** shuffle's loop protect. 50000 cycles containe maximum n7 (factorial 7). */
	protected $cycles;
	protected $cyclesMax = 50000;
    public function shuffle(string $name)
    {
    	$parts = explode(' ', $name);
    	for ($i = 0; $i < count($parts); $i++) {
    		$parts[$i] = trim($parts[$i]);
    	}
    	if (count($parts) < 2) {
    		return $parts;
    	}
    	// $parts = array_values(array_unique($parts));

    	//комбинация из всех слов сразу, а не так, что сначала 1 слово, потом комбинация 2 слов
    	// $parts = [2,2,3,4,5,6,7];
    	$combinations = $this->shuffle1($parts);

    	$pairs = [];
    	for ($i = 0; $i < count($combinations); $i++) {
    		$pairs[] = implode(' ', $combinations[$i]);
    	}
    	$pairs = array_values(array_unique($pairs));
    	// print_r($pairs);die;

    	return $pairs;
    }

    public function shuffle1(array $a)
    {
    	if (count($a) < 2) {
    		return $a;
    	} elseif (count($a) == 2) {
    		return [
    			[$a[0], $a[1]],
    			[$a[1], $a[0]],
    		];
    	}

    	$combinations = [];
    	for ($j = 0; $j < count($a); $j++) {
    		if (++$this->cycles > $this->cyclesMax) {
    			return $combinations;
    		}
	    	$parts = $a;
    		$base = $parts[$j];
    		unset($parts[$j]);
	    	$parts = array_values($parts);

	    	$partsCombinations = $this->shuffle1($parts);
	    	for ($i = 0; $i < count($partsCombinations); $i++) {
	    		if (++$this->cycles > $this->cyclesMax) {
	    			return $combinations;
	    		}
	    		$partsCombination = $partsCombinations[$i];
	    		array_unshift($partsCombination, $base);
	    		$combinations[] = $partsCombination;
	    	}
	    }
	    return $combinations;
    }

    public function setStat($categoryId)
    {
        if (!$categoryId) {
            return false;
        }
        $stat = (new \yii\db\Query())
            ->select([
                'min'              => 'min(price)',
                'max'              => 'max(price)',
                'qtyTotal'         => 'sum(qty)',
                'qtyMax'           => 'max(qty)',
            ])
            ->from(Product::tableName())
            ->where(['catId' => $categoryId])
            ->one();
        if (!$stat) {
            return false;
        }
        $category = Category::findOne($categoryId);
        if (!$category) {
            return false;
        }
        $category->priceMin = $stat['min'];
        $category->priceMax = $stat['max'];
        $category->qtyTotal = $stat['qtyTotal'];
        $category->qtyMax   = $stat['qtyMax'];
        $category->save();
        return true;
    }
}
