<?php

namespace app\models;

class Setup extends \yii\db\Schema
{
    protected function loadTableSchema($name)
    {
        return null;
    }

    public function installSchema()
    {
        $result[] = 'installSchema start';
        $table = 'supplier';
        // $command = \Yii::$app->db->createCommand();
        $command = $this;
        $command->createTable($table, [
            'id' => 'pk',
            // 'title' => 'text not null',
            'title' => SELF::TYPE_STRING,
            'lineStart' => 'smallint',
            'artikul' => 'smallint',
            'title' => 'smallint',
            'price' => 'smallint',
            'qty' => 'smallint',
        ])
        ->execute()
        ;

        $command->createIndex('title', $table, 'title', true)->execute();

        $result[] = 'installSchema end';
        return $result;
    }
}
