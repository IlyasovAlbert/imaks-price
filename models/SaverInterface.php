<?php

namespace app\models;

interface SaverInterface
{
	public function set(string $name, $value);
	public function setNamespace(string $namespace);
	public function getNamespace();
	public function get(string $key = null);
}