<?php

namespace app\models;

class SupplierPriceUpload extends \yii\base\Model
{
    public $supplierId;
    public $price;
    public $extensions = ['csv', 'xlsx'];

    public function rules()
    {
        return [
            [['price'], 'file', /*'skipOnEmpty' => false,*/ 'extensions' => $this->extensions],
            [['supplierId'], 'required'],
            ['supplierId', 'filter', 'filter' => 'intval'],
        ];
    }

    public function isFileValid(string $attribute, \yii\web\UploadedFile $file)
    {
        $maxSizeMb = PriceFile::MAX_SIZE_MB;
        if (!$file->size) {
            $this->addError($attribute, 'Файл не должен иметь нулевой размер');
        } elseif ($file->size > $maxSizeMb*1024*1024) {
            $this->addError($attribute, 'Файл не должен быть больше ' . $maxSizeMb . ' Мб');
        }

        return $this->hasErrors();
    }

    public function attributeLabels()
    {
        return [
            'supplierId' => 'Поставщик',
            'price' => 'Прайс',
        ];
    }
}
