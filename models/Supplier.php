<?php

namespace app\models;

class Supplier extends \yii\db\ActiveRecord
{
    private $priceFile;

    public function getPriceFile()
    {
        if (null === $this->priceFile) {
            $this->priceFile = new PriceFile;
        }
        return $this->priceFile;
    }
    
    public function add(\app\models\SupplierForm $form): bool
    {
        $this->name         = $form->name;
        $this->delivery     = $form->delivery;
        $this->deliveryTime = $form->deliveryTime;
        $this->deliveryCost = $form->deliveryCost;
        $this->deliveryFree = $form->deliveryFree;
        $this->pickup       = $form->pickup;
        $this->discount     = $form->discount;
        $this->addresses    = $form->addresses;
        $this->notes        = $form->notes;

        return $this->save();
    }

    public static function get(int $supplierId)
    {
        return self::findOne(['id' => $supplierId]);
    }

    public static function getName(int $supplierId)
    {
        return self::get($supplierId)['name'];
    }

    public static function getDiscount(int $supplierId)
    {
        return self::get($supplierId)['discount'];
    }

    public function hasFileConfig()
    {
        if (   null !== $this->fileColumnArtikul
            && null !== $this->fileColumnName
            && null !== $this->fileColumnPrice
            && null !== $this->fileColumnQty
            && null !== $this->fileLineDataStart
        ) {
            return true;
        }
        return false;
    }

    public function getFileConfig()
    {
        return [
            'fileColumnArtikul' => $this->fileColumnArtikul,
            'fileColumnName'    => $this->fileColumnName,
            'fileColumnPrice'   => $this->fileColumnPrice,
            'fileColumnQty'     => $this->fileColumnQty,
            'fileLineDataStart' => $this->fileLineDataStart,
            'fileQtyDefault'    => $this->fileQtyDefault,
        ];
    }

    public static function getByName($name)
    {
        return self::findOne(['name' => $name]);
    }

    public function list()
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id',
                'name',
                'delivery',
                'deliveryTime',
                'deliveryCost',
                'deliveryFree',
                'pickup',
                'discount',

                'created_at',
                'updated_at',
            ],
        ]);

        return new \yii\data\ActiveDataProvider([
            'query' => self::find()->orderBy($sort->orders),
            // 'query' => self::find()->joinWith('products')->orderBy($sort->orders),
            'sort' => $sort,
        ]);
    }

    // public function getProducts()
    // {
    //     return $this->hasOne(Product::className(), ['supplierId' => 'id']);
    // }
}
