<?php

namespace app\models;

class CartModel extends \yii\base\BaseObject
{
	protected $helper;

    public function __construct()
    {
    	$this->helper = new CookieModel('cart');
    }

    public function update(int $catId, $qty): CartModel
    {
    	$this->helper->set($catId, $qty);
    	return $this;
    }

    public function delete(int $catId): CartModel
    {
    	$this->helper->set($catId, null);
    	return $this;
    }

    public function get(int $catId = null): array
    {
        $catModel = new \app\models\Category;

        $data = [];
        foreach ($this->helper->get($catId) as $catId => $qty) {
            $cat = $catModel->findOne($catId);
            if (!$cat) {
                $this->helper->set($catId, null);
                continue;
            }

            $data[$catId] = [
                'id' => $cat->id,
                'name' => $cat->name,
                'qty' => $qty,
            ];
        }
        return $data;
    }
}
