<?php

namespace app\models;

class FileRegistryModel extends \yii\base\BaseObject
{
    private static $instance;
    private $files = [];

    private function __construct(){}

    public function gi()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function get(int $fileId)
    {
        if (!isset($this->files[$fileId])) {
            $this->files[$fileId] = PriceFile::get($fileId);
        }
        return $this->files[$fileId];
    }
}