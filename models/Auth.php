<?php

namespace app\models;

class Auth extends \yii\base\BaseObject
{
    public static function mustBeLoggedIn()
    {
        if (!\Yii::$app->user->isGuest) {
            return true;
        }
        \Yii::$app->getResponse()->redirect(['/site/login']);
        return false;
    }
}
