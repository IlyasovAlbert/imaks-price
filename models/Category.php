<?php

namespace app\models;

class Category extends \yii\db\ActiveRecord
{
    const NAME_MIN_LENGTH = 3;
}
