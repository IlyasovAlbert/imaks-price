<?php

namespace app\controllers;

class CartController extends \yii\web\Controller
{
    public function actionJson()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $response = \Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $qty = intval(trim(\Yii::$app->request->get('qty'))) ?: null;
        $catId = trim(\Yii::$app->request->get('catId'));
        $cartModel = new \app\models\CartModel;
        if ($catId && is_numeric($catId)) {
            $cartModel->update($catId, $qty);
        }

        $response->data = $cartModel->get();
    }
}
