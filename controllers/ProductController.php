<?php

namespace app\controllers;

class ProductController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        \Yii::$app->view->title = 'Товары';

        $productModel = new \app\models\ProductModel;
        $supplier = null;
        $conditions = [];

        $supplierId = (int)\Yii::$app->request->get('supplierId');
        if ($supplierId) {
            $supplier = \app\models\Supplier::findOne($supplierId);
        }
        if ($supplier) {
            $conditions['supplierId'] = $supplier->id;
            \Yii::$app->view->title .= ' - Поставщик "' . \yii\helpers\Html::encode($supplier->name) . '"';
        } else {
            $supplierId = null;
        }

        $catId = \Yii::$app->request->get('catId');
        if ($catId) {
            $cat = \app\models\Category::findOne((int)$catId);
        } elseif ($catId === '0') {
            $conditions['catId'] = 0;
            \Yii::$app->view->title .= ' - Без категорий';
        }
        if ($cat) {
            $conditions['catId'] = $cat->id;
            \Yii::$app->view->title .= ' - Категория "' . \yii\helpers\Html::encode($cat->name) . '"';
        } else {
            $catId = null;
        }

        $query = (new \yii\db\Query())
            ->select([
                'product.*',
                'categoryName' => 'category.name',
                'supplierName' => 'supplier.name'
            ])
            ->from(\app\models\Product::tableName())
            ->leftJoin(\app\models\Category::tableName(), 'category.id = product.catId')
            ->leftJoin(\app\models\Supplier::tableName(), 'supplier.id = product.supplierId')
            ->where($conditions)
        ;

        return $this->render('list', [
            'query' => $query,
            'productModel' => $productModel,
            'supplierId' => $supplierId,
            'suppliers' => \app\models\Supplier::find()->all(),
            'catId' => $catId,
        ]);
    }
    
    public function error500()
    {
        \Yii::$app->response->setStatusCode(500);
    }

    public function actionSetCatByName()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $response = \Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        //для like-запроса слова д.б. всегда окружены пробелами
        // $catName = ' ' + $catName + ' ';
        
        $productId = (int)\Yii::$app->request->post('productId');
        $product = \app\models\Product::findOne($productId);
        if (!$product) {
            return $this->error500();
        }
        $oldCategoryId = $product->catId;

        $catName = trim(\Yii::$app->request->post('catName'));
        if (!$catName) {
            $product->catId = null;
            $product->save();
            if ($oldCategoryId) {
                $category = \app\models\CategorySupplierArtikul::findOne([
                    'categoryId' => $oldCategoryId,
                    'supplierId' => $product->supplierId,
                ]);
                $category && $category->delete();
            }
            return $response->data = ['categoryId' => 0, 'categoryName' => ''];
        }
        
        $cat = \app\models\Category::findOne(['name' => $catName]);
        if (!$cat) {
            $cat = new \app\models\Category;
            $cat->name = $catName;
            $cat->save();
        }
        
        $product->catId = $cat->id;
        $product->save();

        \app\models\CategorySupplierArtikulModel::update($product->artikul, $product->supplierId, $product->catId, $oldCategoryId);
        $categoryModel = new \app\models\CategoryModel;
        $categoryModel->setStat($product->catId);
        $categoryModel->setStat($oldCategoryId);

        return $response->data = ['categoryId' => $product->catId, 'categoryName' => $cat->name];
    }
}
