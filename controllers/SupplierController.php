<?php

namespace app\controllers;

class SupplierController extends \yii\web\Controller
{
    public function actionImport()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $result = $this->fileCanUse();
        if (!is_array($result)) {
            return $result;
        }
        [$fileId, $file, $fileModel] = $result;

        //если файл ещё не готов для импорта или всё готово для импорта, но не получена команде об импорте, то отправим на страницу подтверждения
        if (!$file->isReady() || !\Yii::$app->request->get('confirmed')) {
            return \Yii::$app->getResponse()->redirect(['supplier/price-confirm', 'fileId' => $fileId]);
        }

        $productModel = new \app\models\ProductModel;
        if (!$productModel->importFile($file)) {
            return $this->render('priceImport', ['result' => 'importFail']);
        }
        // $file->status = $file::STATUS_IMPORTED;
        $file->save();

        return $this->render('priceImport', [
            'result' => 'importSuccess',
            'supplierId' => $file->supplierId,
            'importStat' => $file->getInfo()['stat'],
        ]);
    }

    public function actionPriceConfirm()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $result = $this->fileCanUse();
        if (!is_array($result)) {
            return $result;
        }
        [$fileId, $file, $fileModel] = $result;

        if (!$file->isLearned()) {
            return \Yii::$app->getResponse()->redirect(['supplier/price-learn', 'fileId' => $fileId]);
        }

        if (\Yii::$app->request->get('confirmed')) {
            $file->status = $file::STATUS_READY;
            $file->save();

            $supplier = (new \app\models\Supplier)->get($file->supplierId);
            $supplier->fileColumnArtikul = $file->fileColumnArtikul;
            $supplier->fileColumnName    = $file->fileColumnName;
            $supplier->fileColumnPrice   = $file->fileColumnPrice;
            $supplier->fileColumnQty     = $file->fileColumnQty;
            $supplier->fileLineDataStart = $file->fileLineDataStart;
            $supplier->fileQtyDefault    = $file->fileQtyDefault;
            $supplier->save();

            return \Yii::$app->getResponse()->redirect(['supplier/import', 'fileId' => $fileId, 'confirmed' => 1]);
        }

        $i=-1;
        $lastViewLine = 49+$file->fileLineDataStart;
        $rows = [];
        foreach ($file->getData() as $row) {
            if (++$i > $lastViewLine) {
                break;
            }
            if ($i < $file->fileLineDataStart) {
                continue;
            }
            $rows[] = $row;
        }
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $rows,
            'pagination' => [
                'pageSize' => 20000,
            ],
        ]);

        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            ['label' => 'Наименование', 'value' => (string)$file->fileColumnName],
            ['label' => 'Артикул', 'value' => (string)$file->fileColumnArtikul],
            ['label' => 'Цена', 'value' => (string)$file->fileColumnPrice],
            ['label' => 'Кол-во', 'value' => (string)$file->fileColumnQty],
        ];

        return $this->render('priceConfirm', [
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'fileId' => $fileId,
            'fileQtyDefault' => [
                'label' => (new \app\models\PriceLearnForm)->attributeLabels()['fileQtyDefault'],
                'value' => (string)$file->fileQtyDefault,
            ],
        ]);
    }

    public function actionPriceLearn()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $result = $this->fileCanUse();
        if (!is_array($result)) {
            return $result;
        }
        [$fileId, $file, $fileModel] = $result;

        $form = new \app\models\PriceLearnForm;
        if (\Yii::$app->request->getIsGet()) {
            $form->fileColumnArtikul = $file->fileColumnArtikul;
            $form->fileColumnName    = $file->fileColumnName;
            $form->fileColumnPrice   = $file->fileColumnPrice;
            $form->fileColumnQty     = $file->fileColumnQty;
            $form->fileLineDataStart = $file->fileLineDataStart;
            $form->fileQtyDefault    = $file->fileQtyDefault;
        }

        if ($form->load(\Yii::$app->request->post()) && $form->validate() && $form->isValsUnique()) {
            $file->setConfig([
                'fileColumnArtikul' => $form->fileColumnArtikul,
                'fileColumnName'    => $form->fileColumnName,
                'fileColumnPrice'   => $form->fileColumnPrice,
                'fileColumnQty'     => $form->fileColumnQty,
                'fileLineDataStart' => $form->fileLineDataStart,
                'fileQtyDefault'    => $form->fileQtyDefault,
            ]);
            $file->status = $file::STATUS_NEED_CONFIRM;
            $file->save();

            return \Yii::$app->getResponse()->redirect(['supplier/price-confirm', 'fileId' => $fileId]);
        }

        $i=0;
        $rows = [];
        foreach ($file->getData() as $row) {
            if (++$i > 50) {
                break;
            }
            $rows[] = $row;
        }
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $rows,
            'pagination' => [
                'pageSize' => 20000,
            ],
        ]);

        $columns[] = ['class' => 'yii\grid\SerialColumn'];
        for ($i=0; $i<count($rows[0]); $i++) {
            $columns[] = (string)$i;
        }

        return $this->render('priceLearn', [
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'model' => $form,
        ]);
    }

    public function actionPriceUpload()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $renderData = [];
        $supplierModel = new \app\models\Supplier();
        $form = new \app\models\SupplierPriceUpload();

        $isFormValid = false;
        if (
            $form->load(\Yii::$app->request->post())
            && $form->validate()
            && $supplierModel->get($form->supplierId)
            && $form->price = \yii\web\UploadedFile::getInstance($form, 'price')
        ) {
            $isFormValid = true;
        }

        if ($isFormValid && !$form->isFileValid('price', $form->price)) {
            $fileId = $supplierModel->getPriceFile()->priceFileAdd($form);

            $supplier = $supplierModel->get($form->supplierId);
            if ($supplier->hasFileConfig()) {
                $file = $supplierModel->getPriceFile()->get($fileId);
                $file->setConfig($supplier->getFileConfig());
                $file->status = $file::STATUS_NEED_CONFIRM;
                $file->save();

                return \Yii::$app->getResponse()->redirect(['supplier/price-confirm', 'fileId' => $fileId]);
            }
            
            return \Yii::$app->getResponse()->redirect(['supplier/price-learn', 'fileId' => $fileId]);
        }

        $renderData['model'] = $form;
        $renderData['suppliers'] = \yii\helpers\ArrayHelper::map($supplierModel::find()->all(), 'id', 'name');
        return $this->render('priceUpload', $renderData);
    }

    public function actionIndex()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $form = new \app\models\SupplierForm();
        $supplier = new \app\models\Supplier();
        return $this->render('list', [
            'suppliers' => $supplier->list(),
            'form' => $form,
        ]);
    }

    public function actionAdd()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $form = new \app\models\SupplierForm();
        $supplierModel = new \app\models\Supplier();

        if ($form->load(\Yii::$app->request->post()) && $form->validate() && $supplierModel->add($form)) {
            ;
            \Yii::$app->session->setFlash('supplierAdded');
            \Yii::$app->session->setFlash('supplierName', $form->name);

            return $this->refresh();
        }

        return $this->render('form', [
            'model' => $form,
        ]);
    }

    public function actionEdit()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $form = new \app\models\SupplierForm();
        $supplierModel = new \app\models\Supplier();

        $supplierId = (int)\Yii::$app->request->get('supplierId');
        if (!$supplierId) {
            return $this->render('error', ['error' => 'Не передан ID поставщика.']);
        }
        $supplier = $supplierModel->get($supplierId);
        if (!$supplier) {
            return $this->render('error', ['error' => 'Поставщик с ID '.$supplierId.' не найден.']);
        }

        $form->id = $supplier->id;

        if (\Yii::$app->request->getIsGet()) {
            $form->name         = $supplier->name;
            $form->delivery     = $supplier->delivery;
            $form->deliveryTime = $supplier->deliveryTime;
            $form->deliveryCost = $supplier->deliveryCost;
            $form->deliveryFree = $supplier->deliveryFree;
            $form->pickup       = $supplier->pickup;
            $form->discount     = $supplier->discount;
            $form->addresses    = $supplier->addresses;
            $form->notes        = $supplier->notes;
        }

        if ($form->load(\Yii::$app->request->post()) && $form->validate()) {
            
            $supplier->name         = $form->name;
            $supplier->delivery     = $form->delivery;
            $supplier->deliveryTime = $form->deliveryTime;
            $supplier->deliveryCost = $form->deliveryCost;
            $supplier->deliveryFree = $form->deliveryFree;
            $supplier->pickup       = $form->pickup;
            $supplier->discount     = preg_replace('/^(\d+)(,{1})(\d*)$/', '$1.$3', $form->discount);
            $supplier->addresses    = $form->addresses;
            $supplier->notes        = $form->notes;
            $supplier->save();

            \Yii::$app->session->setFlash('supplierUpdated');
            \Yii::$app->session->setFlash('supplierName', $form->name);

            return $this->refresh();
        }
        
        return $this->render('form', [
            'model' => $form,
        ]);
    }

    // public function actionSetup()
    // {
    //     $model = new \app\models\Setup();
    //     $result = implode('<br>', $model->installSchema());
    //     return $this->render('page', ['content' => $result]);
    // }

    public function fileCanUse()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $fileId = \Yii::$app->request->get('fileId');
        if (!$fileId) {
            return $this->render('error', ['error' => 'Не передан ID файла.']);
        }

        $fileModel = new \app\models\PriceFile();
        $file = $fileModel->get($fileId);
        if (!$file) {
            return $this->render('error', ['error' => 'Файл не найден.']);
        }

        if ($file->isImported()) {
            return $this->render('error', ['error' => 'Файл уже импортирован в базу данных. Действия над ним недоступны.']);
        }

        return [$fileId, $file, $fileModel];
    }
}
