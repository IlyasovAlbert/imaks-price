<?php

namespace app\controllers;

class CheckoutController extends \yii\web\Controller
{
    public function actionAjax()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $response = \Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $suppliers  = []; //[supplierId => object(supplier)]
        $categories = []; //[categoryId => [name => string, qty => int, itemsBySuppliers => [supplierId => [productId, productId]]]]
        $products   = [];

        $cart = (new \app\models\CartModel)->get();
        if (empty($cart)) {
            $response->data['categories'] = $categories;
            $response->data['suppliers']  = $suppliers;
            $response->data['products']   = $products;
            return;
        }

        //get product's list
        $product = new \app\models\Product;
        $query = $product->find();
        foreach ($cart as $categoryId => $cartItem) {
            $qty = $cartItem['qty'];
            $query->orWhere(['and', ['catId' => $categoryId], ['>=', 'qty', /*$qty*/1], ['state' => $product::STATE_ACTIVE]]);
            $categories[$categoryId] = ['name' => $cartItem['name'], 'qty' => $cartItem['qty']];
        }

        $supplierIds = [];
        //get supplierIds
        foreach ($query->all() as $product) {
            $supplierIds[] = $product->supplierId;
            $categories[$product->catId]['itemsBySuppliers'][$product->supplierId][] = (int)$product->id;
            $productsItem = new \stdClass();
            foreach ($product as $key => $value) {
                $productsItem->$key = $value;
            }
            $products[] = $productsItem;
        }
        $supplierIds = array_values(array_unique($supplierIds));
        natsort($supplierIds);
        $suppliers = array_fill_keys(array_keys(array_flip($supplierIds)), null);

        //get suppliers info
        foreach ( (new \app\models\Supplier)->find()
            ->orWhere(['and', ['id' => $supplierIds]])
            ->all() as $supplier ) {
            $suppliers[$supplier->id] = $supplier;

            foreach ($categories as $categoryId => $category) {
                !isset($categories[$categoryId]['itemsBySuppliers'][$supplier->id])
                    ?  $categories[$categoryId]['itemsBySuppliers'][$supplier->id] = []
                    : null;
            }
        }

        ksort($suppliers);
        foreach ($categories as $categoryId => $category) {
            ksort($categories[$categoryId]['itemsBySuppliers']);
        }

        //set discount price
        foreach ($products as $product) {
            $product->priceOrig = $product->price;
            $product->price = $product->priceOrig * (100-$suppliers[$product->supplierId]->discount)/100;
            $product->priceRounded = round($product->price, 2);
        }

        $response->data['categories'] = $categories;
        $response->data['suppliers']  = $suppliers;
        $response->data['products']   = $products;
    }

    public function actionOrder()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $supplierId = (int)\Yii::$app->request->post('supplierId');
        $productIds =      \Yii::$app->request->post('productIds');
        $pickup     =      \Yii::$app->request->post('pickup');

        $model = new \app\models\CheckoutModel;
        $supplier  = \app\models\Supplier::get($supplierId);
        $filenameWoExt = $supplier->name.' - Заказ от '.date('d.m.Y H-i-s');

        $model->saveSpreadsheetToFile(
            $model->compileSpreadsheet($supplier, $productIds, $pickup),
            $filenameWoExt
        );
    }
}
