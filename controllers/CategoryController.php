<?php

namespace app\controllers;

class CategoryController extends \yii\web\Controller
{
    public function actionSearch()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }

        \Yii::$app->view->title = 'Поиск';

        return $this->render('search', [
            'nameMinLength' => \app\models\Category::NAME_MIN_LENGTH,
        ]);
    }
    public function actionSearchJson()
    {
        if (!\app\models\Auth::mustBeLoggedIn()) {
            return;
        }
        
        $response = \Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $name = trim(\Yii::$app->request->get('name'));
        if (strlen($name) < 3) {
            return;
        }

        $catModel = new \app\models\Category;
        $query = $catModel->find();
        $names = (new \app\models\CategoryModel())->shuffle($name);
        $query->andWhere(['or like', 'name', $names]);
        $query->andWhere(['>=', 'qtyTotal', 1]);
        $response->data = $query->limit(1000)->all();
    }
}
