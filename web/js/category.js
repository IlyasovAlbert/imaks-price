var suppliersClass = {
	suppliers: {},
	delivery : null,
	order    : {},
	stateName: 'suppliersState',

	getSuppliers: function() {
		return this.suppliers;
	},

	getEmptyOrder: function() {
		return {
			amount  : 0,
			delivery: 0,
			total   : 0,
		};
	},

	getEmptySupplierOrder: function() {
		return {
			items     : [],    //массив из productId. Кол-во содержится в productsClass
			positions : 0,     //кол-во наименований
			qty       : 0,     //общее кол-во товаров
			amount    : 0,     //сумма товаров
			pickup    : false, //самовывоз или нет
			delivery  : 0,     //стоимость доставки
			total     : 0,     //итоговая стоимость доставки
		};
	},

	init: function() {
		this.delivery = null;
		this.order    = this.getEmptyOrder();
	},

	setSuppliers: function(suppliers) {
		self = this;
		this.init();
		$.each(suppliers, function(supplierId, supplier) {
			supplier.order = self.getEmptySupplierOrder();
			supplier.deliveryCost = null == supplier.deliveryCost ? 0 : parseFloat(supplier.deliveryCost);
			supplier.deliveryFree = null == supplier.deliveryFree ? 0 : parseFloat(supplier.deliveryFree);

			supplier.pickup   = null == supplier.pickup   ? 0 : parseFloat(supplier.pickup);
			supplier.discount = null == supplier.discount ? 0 : parseFloat(supplier.discount);
			supplier.delivery = null == supplier.delivery ? 0 : parseFloat(supplier.delivery);

			suppliers[supplierId] = supplier;
		});
		this.suppliers = suppliers;
	},

	setDefault: function() {
		this.suppliers = {};
		this.delivery  = null;
	},

	canUse: function(supplierId) {
		supplier = this.suppliers[supplierId];
		return Boolean(!supplier.disabled
			&& (!this.delivery || (this.delivery && supplier.delivery)));
	},

	changeStatus: function(supplierId) {
		this.suppliers[supplierId].disabled = !this.suppliers[supplierId].disabled;
	},

	test: function() {
		suppliers = this.suppliers;
		delivery  = this.delivery;

		this.setDefault();

		this.suppliers = {
			10: {
				delivery: 1,
				pickup: 1,
			},
			20: {
				delivery: 0,
				pickup: 0,
			}
		};

		this.changeStatus(10); //disabled: true
		console.log('---------------Run test');
		console.log('1: ' + (this.canUse(10) === false ? 'ok' : 'fail'));
		console.log('2: ' + (this.canUse(20) === true ? 'ok' : 'fail'));
		this.delivery = true;
		console.log('3: ' + (this.canUse(10) === false ? 'ok' : 'fail'));
		console.log('4: ' + (this.canUse(20) === false ? 'ok' : 'fail'));
		console.log('---------------Test is end');

		this.suppliers = suppliers;
		this.delivery  = delivery;
	},

	calcOrder: function() {
		this.resetOrder();
		this.addItems();
		this.calcAmount();
		this.calcDeliveryAndTotal();
	},

	calcAmount: function() {
		suppliers = this.getSuppliers();
		amount = 0;
		$.each(suppliers, function(supplierId, supplier) {
			order = suppliers[supplierId].order;
			$.each(supplier.order.items, function(index, productId) {
				product = productsClass.getProduct(productId);
				order.amount = product.price * product.qtySelected + order.amount;

				//stats
				order.positions++;
				order.qty = product.qtySelected + order.qty;
			});
			amount = order.amount + amount;
			suppliers[supplierId].order = order;
		});
		this.order.amount = amount;
		this.suppliers = suppliers;
	},

	calcDeliveryAndTotal: function() {
		suppliers = this.getSuppliers();
		delivery = 0;
		total    = 0;
		$.each(suppliers, function(supplierId, supplier) {
			supplier = suppliers[supplierId];
			order    = supplier.order;

			order.delivery = order.amount >= supplier.deliveryFree || order.pickup || order.amount == 0
				? 0
				: supplier.deliveryCost;
			order.total = order.amount + order.delivery;

			suppliers[supplierId].order = order;
			delivery = order.delivery + delivery;
			total    = order.total + total;
		});
		this.order.delivery = delivery;
		this.order.total    = total;
		this.suppliers      = suppliers;
	},

	addItems: function() {
		self = this;
		suppliers = this.getSuppliers();
		$.each(categoriesClass.getCategories(), function(categoryId, category) {
			$.each(category.itemsBySuppliers, function(supplierId, productIds) {
				if (!self.canUse(supplierId)) {
					return;
				}
				$.each(productIds, function(index, productId) {
					product = productsClass.getProduct(productId);
					if (product.qtySelected <= 0) {
						return;
					}
					suppliersClass.suppliers[supplierId].order.items.push(productId);
				});
			});
		});
		this.suppliers = suppliers;
	},

	resetOrder: function() {
		suppliers  = this.getSuppliers();
		self = this;
		this.order = this.getEmptyOrder();
		$.each(suppliers, function(supplierId) {
			pickup = suppliers[supplierId].order.pickup;
			suppliers[supplierId].order = self.getEmptySupplierOrder();
			suppliers[supplierId].order.pickup = pickup;
		});
		this.suppliers = suppliers;
	},

	getOrder: function() {
		return this.order;
	},

	getState: function() {
		suppliers = {};
		$.each(this.getSuppliers(), function(supplierId, supplier){
			if (!supplier.order.pickup && !supplier.disabled) {
				return;
			}
			suppliers[supplierId] = {
				disabled: supplier.disabled,
				order: {
					pickup: supplier.order.pickup
				},
			};
		});

		return {
			suppliers: suppliers,
			delivery : this.delivery,
		};
	},

	setState: function(state) {
		this.delivery = state.delivery;
		for (supplierId in state.suppliers) {
			this.suppliers[supplierId].disabled = state.suppliers[supplierId].disabled;
			this.suppliers[supplierId].order.pickup = state.suppliers[supplierId].order.pickup;
		}
	},

	saveState: function(){
		checkoutHelper.saveState(this.stateName, this.getState());
	},

	loadState: function(){
		state = checkoutHelper.loadState(this.stateName);
		if (!state) {
			return;
		}

		this.setState(state);
	},
};

var checkoutHelper = {
	cartStateName: 'cartState',

	saveState: function(name, state){
		options = {expires: 180*30*24*3600};
		cookieClass.set('data[checkout][' + name + ']', JSON.stringify(state), options);
		cookieClass.set('data[checkout][' + this.cartStateName + ']', cookieClass.get('data[cart]'));
	},

	loadState: function(name){
		if (cookieClass.get('data[checkout][' + this.cartStateName + ']') != cookieClass.get('data[cart]')) {
			return;
		}

		stateEncoded = cookieClass.get('data[checkout][' + name + ']');
		if (stateEncoded == undefined) {
			return;
		}

		state = JSON.parse(stateEncoded);
	  	if (typeof state != 'object') {
	  		return;
	  	}

		return state;
	},
};

var categoriesClass = {
	categories: {},

	getCategory: function(categoryId) {
		return this.categories[categoryId];
	},

	setCategory: function(categoryId, category) {
		this.categories[categoryId] = category;
	},

	getCategories: function() {
		return this.categories;
	},

	setCategories: function(categories) {
		$.each(categories, function(categoryId, category) {
			categories[categoryId].qtyRemaining = categories[categoryId].qty;
			categories[categoryId].items        = [];
		});
		this.categories = categories;
	},

	changeQtyRemaining: function(categoryId, qty) {
		category = this.getCategory(categoryId);

		category.qtyRemaining = category.qtyRemaining + qty;

		this.setCategory(categoryId, category);
	},

	getItems: function(categoryId) {
		return this.getCategory(categoryId).items;
	},

	addItem: function(product) {
		productId  = product.id;
		categoryId = product.catId;

		category = this.getCategory(categoryId);

		//already exists
		if (category.items.indexOf(productId) >= 0) {
			return;
		}
		category.items.push(productId);

		this.setCategory(categoryId, category);
	},

	resetQtyRemaining: function() {
		categories = this.getCategories();

		$.each(categories, function(categoryId, category) {
			categories[categoryId].qtyRemaining = categories[categoryId].qty;
		});

		this.categories = categories;
	},
};

//categoriesClass must be already populated
var productsClass = {
	products: {},

	getProduct: function(productId) {
		return this.products[productId];
	},

	setProduct: function(productId, product) {
		this.products[productId] = product;
	},

	getProducts: function() {
		return this.products;
	},

	setProducts: function(products) {
		var i, product;
		this.products = {};

		for (i = 0; i < products.length; i++) {
			product = products[i];

			//formatting
			product.id    = parseInt(product.id);
			product.qty   = parseInt(product.qty);
			product.priceOrig = parseFloat(product.priceOrig);
			product.price = parseFloat(product.price);
			product.priceRounded = parseFloat(product.priceRounded);

			//add properties
			product.qtySelected = 0;
			product.manual      = false;

			this.setProduct(product.id, product);

			categoriesClass.addItem(product);
		}
	},

	unsetQtyManual: function(productId) {
		product = this.getProduct(productId);

		product.manual      = false;
		product.qtySelected = 0;

		this.setProduct(productId, product);
		this.setQtyAuto();
	},

	setQtyManual: function(productId, qty) {
		product = this.getProduct(productId);

		if (product.qty < qty) {
			return false;
		}

		product.manual      = true;
		product.qtySelected = qty;

		this.setProduct(productId, product);
		this.setQtyAuto();
		return this.getProduct(productId).qtySelected == qty;
	},

	setQtyAuto: function() {
		var products, prices, min, max;
		categoriesClass.resetQtyRemaining();
		this.resetQtyAutoSelected();
		products = this.getProducts();
		$.each(categoriesClass.getCategories(), function(categoryId, category) {
			prices = [];
			productIdsPrice = [];

			$.each(categoriesClass.getItems(categoryId), function(key, productId) {
				var product = products[productId];

				if (suppliersClass.canUse(product.supplierId)) {
					if (product.manual) {
						categoriesClass.changeQtyRemaining(product.catId, -product.qtySelected);
					} else {
						productIdsPrice[product.id] = product.price;
						prices.push(product.price);
					}
				}
			});

			if (prices.length) {
				max = Math.max.apply(null, prices);
				//i < int - condition for loop protection
				for (var i = 0
					;
					i < 1000
					&& prices.length
					&& categoriesClass.getCategory(categoryId).qtyRemaining > 0
					;
					i++)
				{
					min = Math.min.apply(null, prices);
					index = prices.indexOf(min);
					productId = productIdsPrice.indexOf(min);
					//for exclude current productId, increase his price bigger then max price of prices
					productIdsPrice[productId] = max + 1;

					prices.splice(index, 1);

					qty = products[productId].qty < categoriesClass.getCategory(categoryId).qtyRemaining
						? products[productId].qty : categoriesClass.getCategory(categoryId).qtyRemaining;

					products[productId].qtySelected = qty;
					categoriesClass.changeQtyRemaining(products[productId].catId, -qty);
				}
			}
		});
		this.products = products;
		suppliersClass.calcOrder();
	},

	resetQtyAutoSelected: function() {
		products = this.getProducts();
		$.each(products, function(productId, product) {
			//зануляям только автоматически посчитанное кол-во
			if (!product.manual) {
				products[productId].qtySelected = 0;
			}
		});
		this.products = products;
	},

	changeQtyMethod: function(productId) {
		product = this.getProduct(productId);
		product.manual = !product.manual;
		this.setProduct(productId, product);
	},

	getState: function() {
		productsState = {};

		// $.each(this.getProducts(), function(productId, product){
		// 	if (!product.manual) {
		// 		return;
		// 	}
		// 	productsState[productId] = {
		// 		manual     : product.manual,
		// 		qtySelected: product.qtySelected,
		// 	};
		// });

		products = this.getProducts();
		for (productId in products) {
			product = products[productId];
			if (!product.manual) {
				continue;
			}
			productsState[productId] = {
				manual     : product.manual,
				qtySelected: product.qtySelected,
			};
		}

		return productsState;
	},

	setState: function(productsState) {
		self = this;
		$.each(productsState, function(productId, productState){
			product = self.getProduct(productId);
			$.each(productState, function(prop, val){
				product[prop] = val;
			});
			self.setProduct(productId, product);
		});
	},

	saveState: function(){
		checkoutHelper.saveState(this.stateName, this.getState());
	},

	loadState: function(){
		state = checkoutHelper.loadState(this.stateName);
		if (!state) {
			return;
		}

		this.setState(state);
	},
};

var viewClass = {
	debug: !true,

	//return <tr>...</tr>
	renderColumnTitles: function(suppliers) {
		var items = [];
		items.push('<tr><th></th>');
		$.each(suppliers, function(supplierId, supplier) {
			items.push('<th'
				+' class="supplier '+(!suppliersClass.canUse(supplierId) ? 'disabled' : '')+'"'
				+' supplierId='+supplierId
				+'>'
					+supplier.name
				+'</th>'
			);
		});
		items.push('</tr>');
		return items.join('');
	},

	//return <td>...</td>
	renderProductsCell: function(category, products) {
		var items = [];
		var	cellClasses = [];
		var	prices = [];
		$.each(category.itemsBySuppliers, function(supplierId, productIds) {
			cellClasses = [];
			prices      = [];

			!suppliersClass.canUse(supplierId) ? cellClasses.push('disabled') : null;

			if (!productIds.length) {
				prices.push('&mdash;');
			} else {
				selected = false;
				$.each(productIds, function(key, productId) {
					product = products[productId];
					prices.push(
						'<div class=price>' + product.priceRounded
							+ '<div class="name hide">' + product.artikul + '<br>' + product.name + '</div>'
						+ '</div>'
						+ '<input type=checkbox' + (product.manual ? ' checked' : '')
							+ ' class=manual productId=' + product.id
							+ ' ' + (suppliersClass.canUse(supplierId) ? '' : ' disabled') + '>'
						+ ' <input type=number min=0 max=' + product.qty + ' step=1 value="' + product.qtySelected + '" class=qty productId=' + product.id
							+ ' ' + (product.manual && suppliersClass.canUse(supplierId) ? '' : ' disabled') + '>'
						+ (viewClass.debug
							? (
							  '<br>productId=' + product.id
							+ '<br>qty=' + product.qty
							+ '<br>qtySelected=' + product.qtySelected
							)
							: ''
						)
					);
					product.qtySelected ? selected = true : null;
				});
				selected ? cellClasses.push('selected') : null;
			}

				items.push('<td'+(cellClasses.length ? ' class="'+cellClasses.join(' ')+'"' : '')+'>');
				items.push(prices.join('<hr>'));
				items.push('</td>');

		});
		return items.join('');
	},

	//return <tr>...</tr>
	renderCategories: function(categories, products) {
		var items = [];
		$.each(categories, function(categoryId, category) {
			items.push('<tr><th'+(category.qtyRemaining ? ' class=alert' : '')+'>'+category.name
				+(viewClass.debug
					?     '<br><br>categoryId = ' + categoryId
						+ '<br>qty = ' 		      + category.qty
						+ '<br>qtyRemaining = '   + category.qtyRemaining
					: ''
				)
				+'</th>');
			items.push(viewClass.renderProductsCell(category, products));
			items.push('</tr>');
		});
		return items.join('');
	},

	//return <tr>...</tr>
	renderTotal: function(suppliers, grandOrder) {
		var amountRow = [];
		var deliveryFreeRow = [];
		var deliveryCostRow = [];
		var pickupRow = [];
		var deliveryRow = [];
		var totalRow = [];

		amountRow      .push('<tr><th>Итого</th>');
		deliveryFreeRow.push('<tr><th>Бесплатная доставка от</th>');
		deliveryCostRow.push('<tr><th>Платная доставка</th>');
		pickupRow      .push('<tr><th>Самовывоз</th>');
		deliveryRow    .push('<tr><th>Доставка</th>');
		totalRow       .push('<tr><th>Итого с учётом доставки</th>');

		$.each(suppliers, function(supplierId, supplier) {
			order = supplier.order;
			ifDisabled = suppliersClass.canUse(supplierId) ? '' : 'disabled';

			amountRow      .push('<td class="' + ifDisabled + '">' + order.amount + '</td>');
			deliveryFreeRow.push('<td class="' + ifDisabled + '">' + supplier.deliveryFree + '</td>');
			deliveryCostRow.push('<td class="' + ifDisabled + '">' + supplier.deliveryCost + '</td>');

			pickupRow      .push('<td class="pickup ' + ifDisabled
				+(order.pickup ? ' selected' : '') + '" supplierId=' + supplierId + '>'
					+(supplier.pickup ? '+' : '&mdash;')
				+'</td>');

			deliveryRow    .push('<td class="' + ifDisabled + '">' + order.delivery + '</td>');
			totalRow       .push('<td class="' + ifDisabled + '">' + order.total + '</td>');
		});

		amountRow      .push('<th class=totals>' + grandOrder.amount   + '</th></tr>');
		deliveryFreeRow.push('</tr>');
		deliveryCostRow.push('</tr>');
		pickupRow      .push('</tr>');
		deliveryRow    .push('<th class=totals>' + grandOrder.delivery + '</th></tr>');
		totalRow       .push('<th class=totals>' + grandOrder.total    + '</th></tr>');

		return amountRow.join('') + deliveryFreeRow.join('') + deliveryCostRow.join('')
			+ pickupRow.join('') + deliveryRow.join('') + totalRow.join('');
	},

	//return <tr>...</tr>
	renderExcel: function(suppliers, products) {
		var items = [];
		items.push('<tr><th></th>');
		$.each(suppliers, function(supplierId, supplier) {
			//если у поставщика не выбраны товары
			if (!supplier.order.items.length) {
				items.push('<th' + (!suppliersClass.canUse(supplierId) ? ' class="disabled"' : '') + '></th>');
				return;
			}

			items.push('<th>');
			items.push('<form method=post action="'+urlOrder+'">');
			$.each(supplier.order.items, function(index, productId) {
				items.push('<input type=hidden name="productIds['
							+productId+']" value="'+products[productId].qtySelected+'">');
			});
			items.push('<input type=hidden name=supplierId value="'
				+supplierId+'">');
			items.push('<input type=hidden name=pickup value="'
				+(supplier.order.pickup ? 1 : 0)+'">');
			items.push('<input type=image src="/img/excel.png" width=25 value="Скачать">');
			items.push('<input type=hidden name="'
				+yii.getCsrfParam()+'" value="'+yii.getCsrfToken()+'">');
			items.push('</form>');
			items.push('</th>');
		});
		items.push('</tr>');
		return items.join('');
	},

	renderCheckoutTable: function(categories, suppliers, products, order) {
		var rows = [];

		//названия столбцов - это наименования поставщиков
		rows.push(this.renderColumnTitles(suppliers));
		rows.push(this.renderCategories(categories, products));
		rows.push(this.renderTotal(suppliers, order));
		rows.push(this.renderExcel(suppliers, products));

		$table = $('<table/>', {
		'class': 'order',
		html: rows.join('')
		});

		$('#checkout').html($table);
		$('#checkout').prepend('<label><input type=checkbox id="excludeSuppliersWoDelivery"'
			+(suppliersClass.delivery ? ' checked' : '')+'>'
			+' Исключить поставщиков, не имеющих доставку</label>');
	},

	renderCategoryNameSearchResult: function(data){
		var items = [];
		$.each(data, function(key, item){
			items.push(''
				+'<tr>'
					+'<td>' + item.qtyTotal+'</td>'
					+'<td><input type=text class=catId id="catId" catId="'+item.id+'"></td>'
					+'<td>' + item.name + '</td>'
					+'<td>' + item.priceMin + '</td>'
					+'<td>' + item.priceMax + '</td>'
				+'</tr>'
			);
		});
		if (!items.length) {
			$('#result').html('Ничего не найдено');
			return;
		}
		items.unshift(''
			+'<tr>'
				+'<th colspan=2>Кол-во</th>'
				+'<th rowspan=2>Категория</th>'
				+'<th colspan=2>Цена</th>'
			+'</tr>'
			+'<tr>'
				+'<th>Есть</th>'
				+'<th>К заказу</th>'
				+'<th>от</th>'
				+'<th>до</th>'
			+'</tr>'
		);

		$table = $('<table/>', {
		'class': 'catNameResult',
		html: items.join('')
		});

		$('#result').html($table);
	},

	renderCategoryNameHint: function(data, productId){
		var items = [];
		$.each(data, function(key, item){
			items.push(''
				+'<tr>'
					+'<td class="catName" productId=' + productId + '>'    + item.name + '</td>'
					+'<td>Р. ' + item.priceMin + ' - ' + item.priceMax + '</td>'
				+'</tr>'
			);
		});
		if (!items.length) {
			$('.productCategoryNameHint[productId='+productId+']').html('');
			return;
		}

		$table = $('<table/>', {
		'class': 'catNameResult',
		html: items.join('')
		});

		$('.productCategoryNameHint[productId='+productId+']').html($table);
	},
};

var cookieClass = {
	// возвращает cookie с именем name, если есть, если нет, то undefined
	get: function(name) {
	  var matches = document.cookie.match(new RegExp(
	    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	  ));
	  return matches ? decodeURIComponent(matches[1]) : undefined;
	},

	// устанавливает cookie с именем name и значением value
	// options - объект с свойствами cookie (expires, path, domain, secure)
	set: function(name, value, options) {
	  options = options || {};

	  var expires = options.expires;

	  if (typeof expires == "number" && expires) {
	    var d = new Date();
	    d.setTime(d.getTime() + expires * 10);
	    expires = options.expires = d;
	  }
	  if (expires && expires.toUTCString) {
	    options.expires = expires.toUTCString();
	  }

	  value = encodeURIComponent(value);

	  var updatedCookie = name + "=" + value;

	  for (var propName in options) {
	    updatedCookie += "; " + propName;
	    var propValue = options[propName];
	    if (propValue !== true) {
	      updatedCookie += "=" + propValue;
	    }
	  }

	  document.cookie = updatedCookie;
	},

	// удаляет cookie с именем name
	delete: function(name) {
	  setCookie(name, "", {
	    expires: -1
	  })
	},
};

var urls = {
	catNameSearch: null,// '/index.php?r=category%2Fsearch-json',

	getCatNameSearch: function(){
		return this.catNameSearch;
	},

	setCatNameSearch: function(url){
		this.catNameSearch = url;
	},
};

lastCatName = [];
nameMinLength = 3;
$('#catName').on('change paste keyup', function() {
    val = $(this).val().trim();
    if (val.length < nameMinLength || val == lastCatName[0]) {
        return;
    }
    lastCatName[0] = val;
	$("#result").html('загрузка результатов поиска...');

	urls.setCatNameSearch(urlSearch);
    $.getJSON(
        urls.getCatNameSearch(),
        {name: val},
        function(data){
			viewClass.renderCategoryNameSearchResult(data);
		}
    );
    cartRender();
});

$('#search').on('change', '#catId', function() {
	cartRender(this);
});

function cartRender(obj = null)
{
	$('#cart').html('загрузка корзины...');
    url = urlCart;
    // $("#field").html(url);

	requestData = {};
	if (obj) {
		catId = $(obj).attr('catId');
		qty = $(obj).val();
		requestData = {catId: catId, qty: qty};
    	// $("#field").append('&catId='+catId+"&qty="+qty);
    	!qty && $('[catId = ' + catId + ']').val(null);
	}

    $.getJSON(
        url,
        requestData,
		function (data) {
			// var items = ['<caption>Корзина <button id="cartCleanButton">Очистить</button></caption>'];
			var items = ['<caption>Корзина</caption>'];
			$.each( data, function( key, item ) {
				$('[catId = ' + item.id + ']').val(item.qty);
				items.push("<tr>"
					+"<td><input type=text value=" + item.qty + " class=catId id='catId' catId='"+item.id+"'></td>"
					+"<td>" + item.name + "</td>"
					+"</tr>" );
			});
			items.push('<tr><th colspan=2><button id="checkoutButton">Найти поставщиков</button></th></tr>');
			// items.push('<tr><th colspan=2><button id="cartCleanButton">Убрать всё</button></th></tr>');

			$table = $( "<table/>", {
			"class": "catNameResult",
			html: items.join( "" )
			});

			$('#cart').html($table);
		}
    );
}

function round(num)
{
	return num;
}

$('#cart').on("click", '#checkoutButton', function() {
	$("#checkout").html('поиск поставщиков...');

	url = urlCheckout;
    $.getJSON(
        url,
		function(data) {
			categoriesClass.setCategories(data.categories);
			suppliersClass.setSuppliers(data.suppliers);
			suppliersClass.loadState();
			productsClass.setProducts(data.products);
			productsClass.loadState();
			productsClass.setQtyAuto();
			viewClass.renderCheckoutTable(
				categoriesClass.getCategories(),
				suppliersClass.getSuppliers(),
				productsClass.getProducts(),
				suppliersClass.getOrder()
			);
		}
    );
});

$("#checkout").on("click", '.order .supplier', function() {
	supplierId = parseFloat($(this).attr('supplierId'));
	suppliersClass.changeStatus(supplierId);
	suppliersClass.saveState();
	productsClass.setQtyAuto();
	viewClass.renderCheckoutTable(
				categoriesClass.getCategories(),
				suppliersClass.getSuppliers(),
				productsClass.getProducts(),
				suppliersClass.getOrder()
			);
});

$("#checkout").on("click", '.order .pickup', function() {
	supplierId = parseInt($(this).attr('supplierId'));
	supplier   = suppliersClass.suppliers[supplierId];
	//проверим чтобы самовывоз был доступен у поставщика
	if (!supplier.pickup) {
		alert('Для поставщика '+supplier.name+' самовывоз недоступен');
		return;
	}
	suppliersClass.suppliers[supplierId].order.pickup = !suppliersClass.suppliers[supplierId].order.pickup;
	suppliersClass.saveState();
	suppliersClass.calcDeliveryAndTotal();
	viewClass.renderCheckoutTable(
				categoriesClass.getCategories(),
				suppliersClass.getSuppliers(),
				productsClass.getProducts(),
				suppliersClass.getOrder()
			);
});

$("#checkout").on("change", '#excludeSuppliersWoDelivery', function() {
	suppliersClass.delivery = $('#excludeSuppliersWoDelivery').prop('checked') ? true : null;
	suppliersClass.saveState();
	productsClass.setQtyAuto();
	viewClass.renderCheckoutTable(
				categoriesClass.getCategories(),
				suppliersClass.getSuppliers(),
				productsClass.getProducts(),
				suppliersClass.getOrder()
			);
});

$('#checkout').on('click', '.price', function() {
	div = $('.name', this);
	if (div.hasClass('hide')) {
		div.removeClass('hide');
		return;
	}
	div.addClass('hide');
});

$("#checkout").on('change', '.manual', function() {
	productId = $(this).attr('productId');
	productsClass.changeQtyMethod(productId);
	productsClass.saveState();
	manual = productsClass.getProduct(productId).manual;
	$('.qty[productId='+productId+']').attr('disabled', !manual);

	productsClass.setQtyAuto();
	viewClass.renderCheckoutTable(
				categoriesClass.getCategories(),
				suppliersClass.getSuppliers(),
				productsClass.getProducts(),
				suppliersClass.getOrder()
			);
});

$('#checkout').on('change', '.qty', function() {
	productId = $(this).attr('productId');
	qty = parseInt($(this).val().trim());
	productsClass.setQtyManual(productId, qty);
	productsClass.saveState();
	productsClass.setQtyAuto();
	viewClass.renderCheckoutTable(
				categoriesClass.getCategories(),
				suppliersClass.getSuppliers(),
				productsClass.getProducts(),
				suppliersClass.getOrder()
			);
});

////////////

$('#productsList').on('change paste keyup', 'input[name=catName]', function() {
	productId = $(this).attr('productId');
    val = $(this).val().trim();
    if (val.length < nameMinLength || val == lastCatName[productId]) {
        return;
    }
    lastCatName[productId] = val;
	$('.productCategoryNameHint[productId='+productId+']').html('поиск...');

	// urls.setCatNameSearch(urlSearch);
    $.getJSON(
        urls.getCatNameSearch(),
        {name: val},
        function(data){
			viewClass.renderCategoryNameHint(data, productId);
		}
    );
});

$('#productsList').on('click', '.catName', function() {
	productId = $(this).attr('productId');
	val = $(this).text();
	if (val.length >= nameMinLength) {
    	$('input[name=catName][productId=' + productId + ']').val(val);
	}
	$('.productCategoryNameHint[productId='+productId+']').html('');
    $('input[name=catName][productId=' + productId + ']').focus();
});
