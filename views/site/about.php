<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О программе';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Программа разработана для поиска лучших цен товаров у разных поставщиков.
        
    </p>
    <p>
    	Программист: Ильясов Альберт. 8-916-065-34-93. <a href="mailto:kralbert@mail.ru">kralbert@mail.ru</a>
    </p>

</div>
