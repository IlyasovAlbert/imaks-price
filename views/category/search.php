<?php
$jsFile = 'js/category.js';
$this->registerJsFile(
	$jsFile.'?'.filemtime(\Yii::$app->basePath.'/web/'.$jsFile)
    ,['depends' => [\yii\web\JqueryAsset::className()]]
    // ,['position' => yii\web\View::POS_LOAD]
 );

$js = '';
$js .= sprintf(
    'urlSearch = "%s"; nameMinLength = "%s";',
    \yii\helpers\BaseUrl::toRoute(['category/search-json']),
    $nameMinLength
);
$js .= sprintf(
    'urlCart = "%s";',
    \yii\helpers\BaseUrl::toRoute(['cart/json'])
);
$js .= sprintf(
    'urlCheckout = "%s";',
    \yii\helpers\BaseUrl::toRoute(['checkout/ajax'])
);
$js .= sprintf(
    'urlOrder = "%s";',
    \yii\helpers\BaseUrl::toRoute(['checkout/order'])
);
$js .= 'cartRender()';
$this->registerJs($js);

?>
<div class="row" id="search">
	<div class="col-8 col-sm-8" id="searchForm">
		<input type=text id="catName">
		<div id="field"></div>
		<div id="result"></div>
	</div>
	<div class="col-4 col-sm-4">
		<div id="cart"></div>
	</div>
</div>
<div id="checkout"></div>