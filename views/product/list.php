<?php
$jsFile = 'js/category.js';
$this->registerJsFile(
    $jsFile.'?'.filemtime(\Yii::$app->basePath.'/web/'.$jsFile)
    ,['depends' => [\yii\web\JqueryAsset::className()]]
    // ,['position' => yii\web\View::POS_LOAD]
 );

$js .= sprintf(
    'urls.setCatNameSearch("%s");',
    \yii\helpers\BaseUrl::toRoute(['category/search-json'])
);
$this->registerJs($js);

$columns = [];
$columns[] = ['class' => 'yii\grid\SerialColumn'];

if (!$supplierId) {
    $columns[] = [
    	'attribute' => ($attr = 'supplierName'),
    	'label' => $productModel->attributeLabels()[$attr],
        'format' => 'raw',
        'value' => function ($data) {
            $img = \yii\helpers\BaseHtml::tag('img', null, ['src' => '/img/search.png', 'width' => 14]);
            return
                \yii\helpers\BaseHtml::tag('a', $img, [
                    'href' => \yii\helpers\BaseUrl::toRoute(['product/index', 'supplierId' => $data['supplierId']]),
                ])
                .' '.
                \yii\helpers\BaseHtml::tag('span', $data['supplierName'])
                ;
        },
    ];
}

if (!$catId) {
//    $spanCatName = '<span style="border-bottom: 1px dotted; cursor: pointer;" onClick="renderCatNameInput({productId})">{catName}</span>';
//    die(str_replace(['{productId}', '{catName}'], [123, 456], $spanCatName));
    $columns[] = [
        'attribute' => ($attr = 'categoryName'),
        'label' => $productModel->attributeLabels()[$attr],
        'format' => 'raw',
        'value' => function ($data) {
            $img = \yii\helpers\BaseHtml::tag('img', null, ['src' => '/img/search.png', 'width' => 14]);
            return \yii\helpers\BaseHtml::tag(
                'div',
                \yii\helpers\BaseHtml::tag(
                    'span',
                    \yii\helpers\BaseHtml::tag('a', $img, [
                        'href' => \yii\helpers\BaseUrl::toRoute(['product/index', 'catId' => $data['catId']]),
                    ])
                    .' '.
                    \yii\helpers\BaseHtml::tag(
                        'span',

                        ($data['categoryName'] ?: '<span class="not-set">(не задано)</span>'),

                        [
                            'class' => 'categoryInput',
                            'onClick' => 'renderCatNameInput('.$data['id'].', \''.$data['categoryName'].'\')',
                        ]
                    ),
                    [
                        'id' => 'spanCatIdForProduct'.$data['id'],
                    ]
                )
                , [
                    'class' => 'productCategoryName',
                ]
            );
        },
    ];
}

$js = '
function renderCatNameInput(productId, catName)
{
    form = "<form id=\'productCatNameForm\' onSubmit=\'productCatUpdate(this); return false;\'>    "
+"<input type=hidden name=\'productId\' value=\'"+productId+"\'>"
+"<input type=text class=catName name=\'catName\' value=\'"+catName+"\' autofocus productId=\'"+productId+"\'>"

+"</form><div class=productCategoryNameHint productId=\'"+productId+"\'></div>";
    document.getElementById("spanCatIdForProduct"+productId).innerHTML=form;

        
}
function productCatUpdate(obj)
{
    productId = obj.productId.value;
    span = document.getElementById("spanCatIdForProduct"+productId);
    span.innerHTML="сохранение...";
    urlSearch = "'. \yii\helpers\BaseUrl::toRoute(['product/index']) .'";

    var data = $(obj).serialize();
    $.ajax({
    url: "'.\yii\helpers\Url::to(['product/set-cat-by-name']).'",
    type: "POST",
    data: data,
    success: function(res){
        categoryId = res.categoryId;
        categoryName = res.categoryName;

        searchButton = "<a href=\'"+urlSearch+"?catId="+categoryId+"\'><img src=\'/img/search.png\' width=14></a>";
        
        span.innerHTML = searchButton + " <span class=categoryInput onClick=\"renderCatNameInput("+productId+", \'"+categoryName+"\')\">"+(categoryId ? categoryName : "<span class=\"not-set\">(не задано)</span>")+"</span>";
    },
    error: function(){
        span.innerHTML ="<span class=categoryInput onClick=\"renderCatNameInput("+productId+", \'\')\">ошибка!</span>";
    }
    });
    
}
';

$columns = array_merge($columns, [
    [
        'attribute' => ($attr = 'artikul'),
    	'label' => $productModel->attributeLabels()[$attr],
    ],
    [
    	'attribute' => ($attr = 'name'),
    	'label' => $productModel->attributeLabels()[$attr],
    ],
    [
    	'attribute' => ($attr = 'price'),
    	'label' => $productModel->attributeLabels()[$attr],
    ],
    [
    	'attribute' => ($attr = 'qty'),
    	'label' => $productModel->attributeLabels()[$attr],
    ],
]);

//$model = \app\models\ProductCatForm;
?>
<div>
    <h1><?= \yii\helpers\Html::encode($this->title) ?></h1>

        <div class="row" id="productsList">
            <?= \yii\helpers\Html::tag('script', $js) ?>
			<?= \yii\grid\GridView::widget([
			    'dataProvider' => new \yii\data\ActiveDataProvider([
				    'query' => $query,
				    'pagination' => [
				        'pageSize' => 150,
				    ],
				]),
			    'columns' => $columns,
			]); ?>
        </div>

</div>
