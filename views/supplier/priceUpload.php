<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Прайс - Загрузка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-5">

                <p>Загрузите файл в формате <?= implode(', ', $model->extensions) ?>.<!-- Если у вас экселевский файл, 
                выберите "Сохранить как" и во всплывшей форме укажите формат
                csv (разделители запятые). Либо можете воспользоваться любым онлайн-конвертером, например, <a href="#">converter</a>.--></p>

                <?php $form = ActiveForm::begin(['id' => 'supplier-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

                    <?= $form->field($model, 'supplierId')->dropdownList($suppliers, ['prompt' => 'Выберите']); ?>

                    <?= $form->field($model, 'price')->fileInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Загрузить', ['class' => 'btn btn-primary', 'name' => 'supplier-button']) ?>
                    </div>

                <?php $form->end(); ?>

            </div>
        </div>

</div>
