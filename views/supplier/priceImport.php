<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Прайс - Завершение импорта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <?php if ($result == 'importSuccess'): ?>
                <div class="alert alert-success">
                    Файл успешно импортирован.
                </div>
                <?= $importStat['total'] ?>    - Записей всего<br>
                <?= $importStat['imported'] ?> - Импортировано<br>
                <?= $importStat['skipped']?>   - Пропущено <p>
            <?php endif; ?>

            <?php if ($result == 'importFail'): ?>
                <div class="alert alert-warning">
                    Ошибка в процессе импорта файла.
                </div>
            <?php endif; ?>

            <?php if ($result == 'alreadyImported'): ?>
                <div class="alert alert-warning">
                    Файл уже был импортирован ранее.
                </div>
                <?= $importStat['total'] ?>    - Записей всего<br>
                <?= $importStat['imported'] ?> - Импортировано<br>
                <?= $importStat['skipped']?>   - Пропущено <p>
            <?php endif; ?>
        </div>

        <div class="row">
            <?= Html::a('Показать товары без категории', ['product/index', 'supplierId' => $supplierId, 'catId' => 0])?>
        </div>

</div>
