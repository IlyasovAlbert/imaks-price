<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Прайс - Подтверждение';
$this->params['breadcrumbs'][] = $this->title;

$inputVals = [];
foreach ($columns as $column) {
    if (is_string($column) || is_numeric($column)) {
        $inputVals[] = $column;
    }
}
//array_fill
$lineNums = [];
for ($i=1; $i<=50; $i++) {
    $lineNums[] = $i;
}
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-5">
                    <div class="form-group">

                        <?= Html::beginForm(['/supplier/price-learn', 'fileId' => $fileId], 'get', ['style' => 'display: inline;']) ?>
                        <?= Html::submitButton('Переобучить', ['class' => 'btn btn-warning', 'name' => 'supplier-button']) ?>
                        <?= Html::endForm() ?>

                        <?= Html::beginForm(['/supplier/price-confirm', 'fileId' => $fileId], 'get', ['style' => 'display: inline;']) ?>
                        <?= Html::submitButton('Всё верно, импортировать', ['class' => 'btn btn-success', 'name' => 'confirmed', 'value' => 1]) ?>
                        <?= Html::endForm() ?>

                    </div>

            </div>
        </div>

        <div class="row">
            <?= $fileQtyDefault['label'] ?>: <?= $fileQtyDefault['value'] ?: 'не задано' ?>.
            <p>
        </div>
        <div class="row">
                <?= \yii\grid\GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $columns,
                        ]);
                ?>
        </div>

</div>
