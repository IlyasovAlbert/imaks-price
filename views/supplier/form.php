<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Поставщик';
$this->params['breadcrumbs'][] = $this->title;

$yesNo = [0 => 'нет', 1 => 'да'];
$numberFormat = ['type' => 'number', 'min' => 0, 'step' => 1];
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('supplierAdded')): ?>

        <div class="alert alert-success">
            Поставщик <b><?= Html::encode(Yii::$app->session->getFlash('supplierName')) ?></b> успешно добавлен.
        </div>

    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('supplierUpdated')): ?>

        <div class="alert alert-success">
            Поставщик <b><?= Html::encode(Yii::$app->session->getFlash('supplierName')) ?></b> успешно обновлён.
        </div>

    <?php endif; ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'supplier-form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => false]) ?>

                    <?= $form->field($model, 'delivery')->dropdownList($yesNo); ?>

                    <?= $form->field($model, 'deliveryTime'); ?>

                    <?= $form->field($model, 'deliveryCost')->textInput($numberFormat); ?>

                    <?= $form->field($model, 'deliveryFree')->textInput($numberFormat); ?>

                    <?= $form->field($model, 'pickup')->dropdownList($yesNo); ?>

                    <?= $form->field($model, 'discount'); ?>

                    <?= $form->field($model, 'addresses')->textarea(); ?>

                    <?= $form->field($model, 'notes')->textarea(); ?>

                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'supplier-button']) ?>
                    </div>

                <?php $form->end(); ?>

            </div>
        </div>

</div>
