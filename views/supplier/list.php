<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\supplierForm */

use yii\helpers\Html;

$this->title = 'Поставщики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">

			<?= \yii\grid\GridView::widget([
			    'dataProvider' => $suppliers,
            // 'pagination' => [
            //     'pageSize' => 20000,
            // ],
			    'columns' => [
			        ['class' => 'yii\grid\SerialColumn'],
			        // 'id',
			        [
			        	'attribute' => 'id',
						'format' => 'raw',
					    'value' => function ($data) {
					        return Html::a(Html::encode($data->id),['supplier/edit', 'supplierId' => $data->id]);
					    },
			        ],
			        [
			        	'attribute' => ($attr = 'id'),
			        	'label' => 'Наименования',
						'format' => 'raw',
					    'value' => function ($data) {
					        return Html::a('все',['product/index', 'supplierId' => $data->id, 'catId' => ''])
					        	. ' | ' . Html::a('без категорий',['product/index', 'supplierId' => $data->id, 'catId' => 0]);
					    },
			        ],
			        // [
			        // 	'attribute' => ($attr = 'productsQty'),
			        // 	'label' => 'Наименований',
			        // 	'format' => 'number',
			        // ],
			        // [
			        // 	'attribute' => ($attr = 'productsSkippedQty'),
			        // 	'label' => 'Пропущенных наименований',
			        // 	'format' => 'number',
			        // ],
			        // [
			        // 	'attribute' => ($attr = 'uploadDate'),
			        // 	'label' => 'Дата последней загрузки',
			        // 	'format' => 'date',
			        // ],
			        // [
			        // 	'attribute' => ($attr = \app\models\Product::className().'.name'),
			        // 	'label' => 'Дата последней загрузки',
			        // 	'format' => 'date',
			        // ],
			        [
			        	'attribute' => ($attr = 'name'),
			        	'label' => $form->attributeLabels()[$attr],
			        ],
			        [
			        	'attribute' => ($attr = 'delivery'),
			        	'label' => $form->attributeLabels()[$attr],
			        	'format' => 'boolean',
			        ],
			        [
			        	'attribute' => ($attr = 'deliveryTime'),
			        	'label' => $form->attributeLabels()[$attr],
			        ],
			        [
			        	'attribute' => ($attr = 'deliveryCost'),
			        	'label' => $form->attributeLabels()[$attr],
			        ],
			        [
			        	'attribute' => ($attr = 'deliveryFree'),
			        	'label' => $form->attributeLabels()[$attr],
			        ],
			        [
			        	'attribute' => ($attr = 'pickup'),
			        	'label' => $form->attributeLabels()[$attr],
			        	'format' => 'boolean',
			        ],
			        [
			        	'attribute' => ($attr = 'discount'),
			        	'label' => $form->attributeLabels()[$attr],
			        	'format' => 'decimal',
			        ],
			        [
			        	'attribute' => ($attr = 'addresses'),
			        	'label' => $form->attributeLabels()[$attr],
			        ],
			        [
			        	'attribute' => ($attr = 'notes'),
			        	'label' => $form->attributeLabels()[$attr],
			        ],
			        [
			        	'attribute' => ($attr = 'createdAt'),
			        	'label' => $form->attributeLabels()[$attr],
			        	'format' => ['date', 'php:d.m.Y']
			        ],
			        [
			        	'attribute' => ($attr = 'updatedAt'),
			        	'label' => $form->attributeLabels()[$attr],
			        	'format' => ['date', 'php:d.m.Y']
			        ],
			    ],
			]); ?>
            
        </div>

</div>
