<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Прайс - Обучение';
$this->params['breadcrumbs'][] = $this->title;

$inputVals = [];
foreach ($columns as $column) {
    if (is_string($column) || is_numeric($column)) {
        $inputVals[] = $column;
    }
}
//array_fill
$lineNums = [];
for ($i=1; $i<=50; $i++) {
    $lineNums[] = $i;
}
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'fileColumnArtikul')->dropdownList($inputVals, ['prompt' => 'Укажите номер столбца']) ?>
                    <?= $form->field($model, 'fileColumnName')->dropdownList($inputVals, ['prompt' => 'Укажите номер столбца']) ?>
                    <?= $form->field($model, 'fileColumnPrice')->dropdownList($inputVals, ['prompt' => 'Укажите номер столбца']) ?>
                    <?= $form->field($model, 'fileColumnQty')->dropdownList($inputVals, ['prompt' => 'Укажите номер столбца']) ?>
                    <?= $form->field($model, 'fileLineDataStart')->dropdownList($lineNums, ['prompt' => 'Укажите номер строки']) ?>
                    <?= $form->field($model, 'fileQtyDefault') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Проверить', ['class' => 'btn btn-primary', 'name' => 'supplier-button']) ?>
                    </div>

                <?php $form->end(); ?>

            </div>
        </div>

        <div class="row">
                <?= \yii\grid\GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $columns,
                        ]);
                ?>
        </div>

</div>
